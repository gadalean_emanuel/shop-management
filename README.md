This is an app for store management, which contains a login and a CRUD functionalities for: employees, managers and admins. 
Saves you time and energy so that you can focus on your customers. Make your market business run more efficient!

What do you need to run this project:

[Java 13](https://www.oracle.com/java/technologies/javase/jdk13-archive-downloads.html)

[JavaFX 11](https://openjfx.io/)

[MySql 8.0.26](https://www.mysql.com/)