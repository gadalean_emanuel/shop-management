package app;

import app.controller.Controller;
import app.repository.*;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import app.view.LoginView;

import java.io.FileInputStream;

// TODO: peste tot unde updatezi shopid si nu e necesar modidfica, cand manageru face update la un encashment tre sa updatezi depositu
public class Main extends Application {

    public static Stage window;

    @Override
    public void start(Stage window) throws Exception {
        Main.window = window;

        window.setTitle("Lidl");

        Image logoImage = new Image(new FileInputStream("C:\\Users\\Emanuel Gadalean\\Links\\lidl.png"));
        window.getIcons().add(logoImage);

        UsersRepository usersRepository = new UsersRepository();
        ShopRepository shopRepository = new ShopRepository(usersRepository);
        EncashmentRepository encashmentRepository = new EncashmentRepository();
        ExpenseRepository expenseRepository = new ExpenseRepository();
        ProductRepository productRepository = new ProductRepository();
        Controller controller = new Controller(usersRepository, shopRepository, encashmentRepository, expenseRepository, productRepository);

        window.setScene(new Scene(new LoginView(controller).display(), 260, 300));

        window.show();
    }


    public static void main(String[] args) {
        launch(args);
    }
}
