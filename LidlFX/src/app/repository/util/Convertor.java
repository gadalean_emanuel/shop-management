package app.repository.util;


import app.model.Product;
import app.model.User;
import javafx.scene.control.ComboBox;
import javafx.util.StringConverter;

public class Convertor {

    public static void convertProductsComboDisplayList(ComboBox<Product> productComboBox) {
        productComboBox.setConverter(new StringConverter<>() {
            @Override
            public String toString(Product product) {
                if (product == null) return "";
                return product.getName() != null ? product.getName() + "  - " + product.getCost() + " Lei" : "";
            }

            @Override
            public Product fromString(final String string) {
                return productComboBox.getItems().stream().filter(product -> product.getName().equals(string)).findFirst().orElse(null);
            }
        });
    }

    public static void convertManagersComboDisplayList(ComboBox<User> managersComboBox) {
        managersComboBox.setConverter(new StringConverter<>() {
            @Override
            public String toString(User user) {
                if (user == null) return "";
                return user.getName() != null ? user.getName() : "";
            }

            @Override
            public User fromString(final String string) {
                return managersComboBox.getItems().stream().filter(user -> user.getName().equals(string)).findFirst().orElse(null);
            }
        });
    }
}
