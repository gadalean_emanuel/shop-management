package app.repository;

import app.model.Expense;
import app.repository.util.DBConnection;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class ExpenseRepository {

    public List<Expense> findAll() {
        List<Expense> expenses = new ArrayList<>();
        String sql = "SELECT * FROM expenses";
        try (Connection connection = DBConnection.getInstance().get(); PreparedStatement statement = connection.prepareStatement(sql); ResultSet resultSet = statement.executeQuery()) {
            while (resultSet.next()) {
                int id = resultSet.getInt("id");
                String name = resultSet.getString("name");
                float cost = resultSet.getFloat("cost");
                Date date = resultSet.getDate("date");
                int shopId = resultSet.getInt("shop_id");
                expenses.add(new Expense(id, name, cost, date.toLocalDate(), shopId));
            }

        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
        }
        return expenses;
    }

    public void deleteExpense(Expense expense) {
        String sql = "DELETE FROM expenses where id = ?";
        try (Connection connection = DBConnection.getInstance().get(); PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setInt(1, expense.getId());
            statement.execute();
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
        }
    }

    public void insertExpense(Expense expense) {
        String sql = "INSERT INTO expenses (id, name, cost, date, shop_id) VALUES (?, ?, ?, ?, ?)";
        try (Connection connection = DBConnection.getInstance().get(); PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setInt(1, findAll().size() + 1);
            statement.setString(2, expense.getName());
            statement.setFloat(3, expense.getCost());
            statement.setDate(4, Date.valueOf(expense.getDate()));
            statement.setInt(5, expense.getShopId());
            statement.execute();
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
        }

    }

    public void updateExpense(Expense expense) {
        String sql = "UPDATE expenses SET id = ?, name = ?, cost = ?, date = ?, shop_id = ? where id = ?";
        try (Connection connection = DBConnection.getInstance().get(); PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setInt(1, expense.getId());
            statement.setString(2, expense.getName());
            statement.setFloat(3, expense.getCost());
            statement.setDate(4, Date.valueOf(expense.getDate()));
            statement.setInt(5, expense.getShopId());
            statement.setInt(6, expense.getId());
            statement.execute();
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
        }

    }

    public Expense findExpenseById(int expenseId) {
        for (Expense expense : findAll()) {
            if (expense.getId() == expenseId) {
                return expense;
            }
        }
        return null;
    }

    public List<Expense> findAllByShopId(int shopId) {
        String sql = "SELECT * FROM expenses where shop_id = " + shopId;
        List<Expense> expenses = new ArrayList<>();
        try (Connection connection = DBConnection.getInstance().get(); PreparedStatement statement = connection.prepareStatement(sql); ResultSet resultSet = statement.executeQuery()) {
            while (resultSet.next()) {
                int id = resultSet.getInt("id");
                String name = resultSet.getString("name");
                float cost = resultSet.getFloat("cost");
                Date date = resultSet.getDate("date");
                int shopIdField = resultSet.getInt("shop_id");
                expenses.add(new Expense(id, name, cost, date.toLocalDate(), shopIdField));
            }

        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
        }
        return expenses;
    }
}
