package app.repository;

import app.model.Shop;
import app.model.User;
import app.repository.constants.UserRoles;
import app.repository.util.DBConnection;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ShopRepository {

    private UsersRepository usersRepository;

    public ShopRepository(UsersRepository usersRepository) {
        this.usersRepository = usersRepository;
    }


    public List<Shop> findAll() {
        List<Shop> shops = new ArrayList<>();
        try (Connection connection = DBConnection.getInstance().get(); PreparedStatement statement = connection.prepareStatement("SELECT * FROM SHOPS"); ResultSet resultSet = statement.executeQuery()) {
            while (resultSet.next()) {
                int id = resultSet.getInt("id");
                String name = resultSet.getString("name");
                String address = resultSet.getString("address");
                shops.add(new Shop(id, name, address, findAllEmployeesByShopId(id), findAllManagersByShopId(id)));
            }
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
        }
        return shops;
    }

    public List<User> findAllManagersByShopId(int shopId) {
        List<User> managers = new ArrayList<>();
        String sql = "SELECT user_id FROM shop_users where shop_id = " + shopId;
        try (Connection connection = DBConnection.getInstance().get(); PreparedStatement statement = connection.prepareStatement(sql); ResultSet resultSet = statement.executeQuery()) {
            while (resultSet.next()) {
                int userId = resultSet.getInt("user_id");
                User user = usersRepository.findById(userId);
                if (user.getUserRole().equals(UserRoles.Manager.name())) {
                    managers.add(user);
                }
            }
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
        }
        return managers;
    }

    public List<User> findAllEmployeesByShopId(int shopId) {
        List<User> employees = new ArrayList<>();
        String sql = "SELECT user_id FROM shop_users where shop_id = " + shopId;
        try (Connection connection = DBConnection.getInstance().get(); PreparedStatement statement = connection.prepareStatement(sql); ResultSet resultSet = statement.executeQuery()) {
            while (resultSet.next()) {
                int userId = resultSet.getInt("user_id");
                User user = usersRepository.findById(userId);
                if (user.getUserRole().equals(UserRoles.Employee.name())) {
                    employees.add(user);
                }
            }
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
        }
        return employees;
    }


    public Shop findById(int shopId) {
        String sql = "SELECT * FROM shops where shopId = " + shopId;
        try (Connection connection = DBConnection.getInstance().get(); PreparedStatement statement = connection.prepareStatement(sql); ResultSet resultSet = statement.executeQuery()) {
            resultSet.next();
            int id = resultSet.getInt("shopId");
            String name = resultSet.getString("name");
            String address = resultSet.getString("address");
            return new Shop(id, name, address, findAllEmployeesByShopId(id), findAllManagersByShopId(id));

        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
        }
        return null;
    }


    public void insert(Shop entity) {
        String sql = "INSERT INTO shops (id, name, address) VALUES (?, ?, ?)";
        try (Connection connection = DBConnection.getInstance().get(); PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setInt(1, findAll().size() + 1);
            statement.setString(2, entity.getName());
            statement.setString(3, entity.getAddress());
            statement.execute();
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
        }
    }

    public void updateShopUsersRelation(Shop shop) {
        List<User> oldShopStaffList = getActualStaffList(shop);

        List<User> updatedShopStaffList = getUpdateStaffList(shop);

        for (User oldStaffMember : oldShopStaffList) {
            if (!updatedShopStaffList.contains(oldStaffMember)) {
                deleteShopUserRelation(shop.getId(), oldStaffMember.getId());
            }
        }

        for (User newStaffMember : updatedShopStaffList) {
            if (!oldShopStaffList.contains(newStaffMember)) {
                insertShopUserRelation(shop.getId(), newStaffMember.getId());
            }
        }
    }

    private List<User> getUpdateStaffList(Shop shop) {
        List<User> newManagersList = shop.getManagers();
        List<User> newEmployeesList = shop.getEmployees();

        List<User> newStaffList = new ArrayList<>();
        newStaffList.addAll(newManagersList);
        newStaffList.addAll(newEmployeesList);
        return newStaffList;
    }

    private List<User> getActualStaffList(Shop shop) {
        List<User> oldEmployeesList = findAllEmployeesByShopId(shop.getId());
        List<User> oldManagersList = findAllManagersByShopId(shop.getId());

        List<User> oldShopStaffList = new ArrayList<>();
        oldShopStaffList.addAll(oldManagersList);
        oldShopStaffList.addAll(oldEmployeesList);
        return oldShopStaffList;
    }

    private void deleteShopUserRelation(int shopId, int userId) {
        String sql = "DELETE FROM shop_users WHERE shop_id = ? and user_id = ?";
        try (Connection connection = DBConnection.getInstance().get(); PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setInt(1, shopId);
            statement.setInt(2, userId);
            statement.execute();
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
        }
    }

    private void insertShopUserRelation(int shopId, int userId) {
        String sql = "INSERT INTO shop_users (id, shop_id, user_id) VALUES (?, ?, ?)";
        try (Connection connection = DBConnection.getInstance().get(); PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setInt(1, findNextShopRelationId());
            statement.setInt(2, shopId);
            statement.setInt(3, userId);
            statement.execute();
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
        }
    }

    private int findNextShopRelationId() {
        String sql = "SELECT * FROM shop_users";
        try (Connection connection = DBConnection.getInstance().get(); PreparedStatement statement = connection.prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE); ResultSet resultSet = statement.executeQuery()) {
            resultSet.last();
            return resultSet.getInt("id") + 1;
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
        }
        return 1;
    }

    public void updateEmployeeFromShop(Shop shop, User employee) {
        List<User> shopEmployees = findAllEmployeesByShopId(shop.getId());
        for (int i = 0; i < shopEmployees.size(); i++) {
            if (shopEmployees.get(i).getId() == employee.getId()) {
                shopEmployees.set(i, employee);
            }
        }
    }

    public void updateManagerFromShop(Shop shop, User manager) {
        List<User> shopManagers = findAllManagersByShopId(shop.getId());
        for (int i = 0; i < shopManagers.size(); i++) {
            if (shopManagers.get(i).getId() == manager.getId()) {
                shopManagers.set(i, manager);
            }
        }
    }

    public void update(Shop shop) {
        String sql = "UPDATE shops SET id = ?, name = ?, address = ? where id = " + shop.getId();
        try (Connection connection = DBConnection.getInstance().get(); PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setInt(1, shop.getId());
            statement.setString(2, shop.getName());
            statement.setString(3, shop.getAddress());
            statement.execute();
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
        }
    }

}

