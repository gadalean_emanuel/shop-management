package app.repository.constants;

import java.time.format.DateTimeFormatter;

/**
 * Used for date formatting after DATE_FORMAT pattern
 */
public class DateFormat {

    public static final String DATE_FORMAT = "dd.MM.yyyy";
    public static final DateTimeFormatter DATE_FORMATTER = DateTimeFormatter.ofPattern(DATE_FORMAT);
}
