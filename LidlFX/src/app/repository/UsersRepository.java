package app.repository;

import app.model.User;
import app.repository.util.DBConnection;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * CRUD for users
 */
public class UsersRepository {

    public List<User> findAll() {
        List<User> users = new ArrayList<>();
        String sql = "SELECT * FROM USERS";
        try (Connection connection = DBConnection.getInstance().get(); PreparedStatement statement = connection.prepareStatement(sql); ResultSet resultSet = statement.executeQuery()) {
            while (resultSet.next()) {
                int id = resultSet.getInt("id");
                String name = resultSet.getString("name");
                int age = resultSet.getInt("age");
                long cnp = resultSet.getLong("cnp");
                String gender = resultSet.getString("gender");
                String address = resultSet.getString("address");
                String username = resultSet.getString("username");
                String password = resultSet.getString("user_password");
                String user_role = resultSet.getString("user_role");
                String user_status = resultSet.getString("user_status");
                users.add(new User(id, name, age, cnp, gender, address, username, password, user_role, user_status));
            }

        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
        }
        return users;
    }

    public User findById(int userId) {
        String sql = "SELECT * FROM users where id = " + userId;
        try (Connection connection = DBConnection.getInstance().get(); PreparedStatement statement = connection.prepareStatement(sql); ResultSet resultSet = statement.executeQuery()) {
            resultSet.next();
            int id = resultSet.getInt("id");
            String name = resultSet.getString("name");
            int age = resultSet.getInt("age");
            long cnp = resultSet.getLong("cnp");
            String gender = resultSet.getString("gender");
            String address = resultSet.getString("address");
            String username = resultSet.getString("username");
            String password = resultSet.getString("user_password");
            String user_role = resultSet.getString("user_role");
            String user_status = resultSet.getString("user_status");
            return new User(id, name, age, cnp, gender, address, username, password, user_role, user_status);
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
        }
        return null;
    }


    public void insert(User newUser) {
        String sql = "INSERT INTO users (id, name, age, cnp, gender, address, username, user_password, user_role, user_status) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
        try (Connection connection = DBConnection.getInstance().get(); PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setInt(1, newUser.getId());
            statement.setString(2, newUser.getName());
            statement.setInt(3, newUser.getAge());
            statement.setLong(4, newUser.getCNP());
            statement.setString(5, newUser.getGender());
            statement.setString(6, newUser.getAddress());
            statement.setString(7, newUser.getUsername());
            statement.setString(8, newUser.getPassword());
            statement.setString(9, newUser.getUserRole());
            statement.setString(10, newUser.getUserStatus());
            statement.execute();
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
        }
    }

    public void delete(User user) {
        String sql = "DELETE FROM users WHERE id = ?";
        try (Connection connection = DBConnection.getInstance().get(); PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setInt(1, user.getId());
            statement.execute();
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
        }
    }

    public void update(User user) {
        String sql = "UPDATE users set id = ?, name = ?, age = ?, cnp = ?, gender = ?, address = ?, username = ?, user_password = ?, user_role = ?, user_status = ? where id = ?";
        try (Connection connection = DBConnection.getInstance().get(); PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setInt(1, user.getId());
            statement.setString(2, user.getName());
            statement.setInt(3, user.getAge());
            statement.setLong(4, user.getCNP());
            statement.setString(5, user.getGender());
            statement.setString(6, user.getAddress());
            statement.setString(7, user.getUsername());
            statement.setString(8, user.getPassword());
            statement.setString(9, user.getUserRole());
            statement.setString(10, user.getUserStatus());
            statement.setInt(11, user.getId());
            statement.execute();
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
        }
    }
}
