package app.repository;

import app.model.Product;
import app.repository.util.DBConnection;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class ProductRepository {

    public List<Product> findAll() {
        List<Product> products = new ArrayList<>();
        String sql = "SELECT * FROM PRODUCTS";
        try (Connection connection = DBConnection.getInstance().get(); PreparedStatement statement = connection.prepareStatement(sql); ResultSet resultSet = statement.executeQuery()) {
            while (resultSet.next()) {
                int id = resultSet.getInt("id");
                String name = resultSet.getString("name");
                float cost = resultSet.getFloat("cost");
                String unit = resultSet.getString("unit");
                int quantity_in_containers = resultSet.getInt("quantity_in_containers");
                int shopId = resultSet.getInt("shop_id");
                products.add(new Product(id, name, cost, unit, quantity_in_containers, shopId));
            }
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
        }
        return products;
    }

    public List<Product> findAllByShopId(int shopId) {
        List<Product> products = new ArrayList<>();
        String sql = "SELECT * FROM PRODUCTS where shop_id = " + shopId;
        try (Connection connection = DBConnection.getInstance().get(); PreparedStatement statement = connection.prepareStatement(sql); ResultSet resultSet = statement.executeQuery()) {
            while (resultSet.next()) {
                int id = resultSet.getInt("id");
                String name = resultSet.getString("name");
                float cost = resultSet.getFloat("cost");
                String unit = resultSet.getString("unit");
                int quantity_in_containers = resultSet.getInt("quantity_in_containers");
                int shopIdField = resultSet.getInt("shop_id");
                products.add(new Product(id, name, cost, unit, quantity_in_containers, shopIdField));
            }
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
        }
        return products;
    }

    public void insert(Product product) {
        String sql = "INSERT INTO products (id, name, cost, unit, quantity_in_containers, shop_id) VALUES (?, ?, ?, ?, ?, ?)";
        try (Connection connection = DBConnection.getInstance().get(); PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setInt(1, findAll().size() + 1);
            statement.setString(2, product.getName());
            statement.setFloat(3, product.getCost());
            statement.setString(4, product.getUnit());
            statement.setInt(5, product.getQuantityInContainers());
            statement.setInt(6, product.getShopId());
            statement.execute();
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
        }
    }

    public void delete(Product entity) {
        String sql = "DELETE FROM products where id = ?";
        try (Connection connection = DBConnection.getInstance().get(); PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setInt(1, entity.getId());
            statement.execute();
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
        }
    }

    public void update(Product product) {
        String sql = "UPDATE products SET id = ?, name = ?, cost = ?, unit = ?, quantity_in_containers = ?, shop_id = ? where id = ?";
        try (Connection connection = DBConnection.getInstance().get(); PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setInt(1, product.getId());
            statement.setString(2, product.getName());
            statement.setFloat(3, product.getCost());
            statement.setString(4, product.getUnit());
            statement.setInt(5, product.getQuantityInContainers());
            statement.setInt(6, product.getShopId());
            statement.setInt(7, product.getId());
            statement.execute();
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
        }
    }

    public Product findProductByShopIdAndByProductName(int shopId, String productName) {
        Optional<Product> searchedProduct = findAllByShopId(shopId).stream().filter(product -> product.getName().equals(productName)).findFirst();
        return searchedProduct.isEmpty() ? null : searchedProduct.get();
    }

}
