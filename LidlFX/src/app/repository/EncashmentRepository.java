package app.repository;

import app.model.Encashment;
import app.repository.util.DBConnection;


import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class EncashmentRepository {

    public List<Encashment> findAllEncashments() {
        List<Encashment> encashments = new ArrayList<>();
        String sql = "SELECT * FROM ENCASHMENTS";
        try (Connection connection = DBConnection.getInstance().get(); PreparedStatement preparedStatement = connection.prepareStatement(sql); ResultSet resultSet = preparedStatement.executeQuery()) {
            while (resultSet.next()) {
                int id = resultSet.getInt("id");
                float value = resultSet.getFloat("value");
                String product_name = resultSet.getString("product_name");
                int num_of_prod = resultSet.getInt("num_of_products");
                Date date = resultSet.getDate("date");
                int shopId = resultSet.getInt("shop_id");
                encashments.add(new Encashment(id, value, product_name, num_of_prod, date.toLocalDate(), shopId));
            }
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
        }
        return encashments;
    }

    public List<Encashment> findAllByShopId(int shopId) {
        List<Encashment> encashments = new ArrayList<>();
        String sql = "SELECT * FROM encashments where shop_id = " + shopId;
        try (Connection connection = DBConnection.getInstance().get(); PreparedStatement statement = connection.prepareStatement(sql); ResultSet resultSet = statement.executeQuery()) {
            while (resultSet.next()) {
                int id = resultSet.getInt("id");
                float value = resultSet.getFloat("value");
                String product_name = resultSet.getString("product_name");
                int num_of_prod = resultSet.getInt("num_of_products");
                Date date = resultSet.getDate("date");
                int shopIdField = resultSet.getInt("shop_id");
                encashments.add(new Encashment(id, value, product_name, num_of_prod, date.toLocalDate(), shopIdField));
            }

        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
        }
        return encashments;
    }

    public void deleteEncashment(Encashment encashment) {
        String sql = "DELETE FROM encashments where id = ?";
        try (Connection connection = DBConnection.getInstance().get(); PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setInt(1, encashment.getId());
            statement.execute();
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
        }
    }

    public void updateEncashment(Encashment encashment) {
        String sql = "UPDATE encashments SET id = ?, value = ?, product_name = ?, num_of_products = ?, date = ?, shop_id = ? where id = ?";
        try (Connection connection = DBConnection.getInstance().get(); PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setInt(1, encashment.getId());
            statement.setFloat(2, encashment.getValue());
            statement.setString(3, encashment.getProductName());
            statement.setInt(4, encashment.getNumOfProducts());
            statement.setDate(5, Date.valueOf(encashment.getDate()));
            statement.setInt(6, encashment.getShopId());
            statement.setInt(7, encashment.getId());
            statement.execute();
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
        }
    }

    public Encashment findEncashmentById(int id) {
        for (Encashment encashment : findAllEncashments()) {
            if (encashment.getId() == id) {
                return encashment;
            }
        }
        return null;
    }

    public void insertEncashment(Encashment encashment) {
        String sql = "INSERT INTO encashments (id, value, product_name, num_of_products, date, shop_id) VALUES (?, ?, ?, ?, ?, ?)";
        try (Connection connection = DBConnection.getInstance().get(); PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setInt(1, findAllEncashments().size() + 1);
            statement.setFloat(2, encashment.getValue());
            statement.setString(3, encashment.getProductName());
            statement.setInt(4, encashment.getNumOfProducts());
            statement.setDate(5, Date.valueOf(encashment.getDate()));
            statement.setInt(6, encashment.getShopId());
            statement.execute();
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
        }
    }


}
