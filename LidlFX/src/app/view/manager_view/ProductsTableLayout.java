package app.view.manager_view;

import app.Main;
import app.controller.Controller;
import app.model.Product;
import app.model.Shop;
import app.model.User;
import app.view.util.ButtonBeautification;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.List;

public class ProductsTableLayout extends ManagerView {

    private TableView<Product> productTableView;

    private TextField nameField;

    private TextField costField;

    private TextField unitField;

    private TextField quantityField;

    public ProductsTableLayout(Controller controller) {
        super(controller);
    }

    public Node buildProductsTable(Shop shop) {
        productTableView = new TableView<>();

        TableColumn<Product, Integer> productIdColumn = new TableColumn<>("Id");
        productIdColumn.setCellValueFactory(new PropertyValueFactory<>("id"));
        productIdColumn.setMinWidth(25);

        TableColumn<Product, String> nameColumn = new TableColumn<>("Name");
        nameColumn.setCellValueFactory(new PropertyValueFactory<>("name"));
        nameColumn.setMinWidth(150);

        TableColumn<Product, Double> costColumn = new TableColumn<>("Price");
        costColumn.setCellValueFactory(new PropertyValueFactory<>("cost"));
        costColumn.setMinWidth(150);

        TableColumn<Product, String> unitColumn = new TableColumn<>("Unit");
        unitColumn.setCellValueFactory(new PropertyValueFactory<>("unit"));
        unitColumn.setMinWidth(100);

        TableColumn<Product, Integer> quantityInContainersColumn = new TableColumn<>("Quantity in containers");
        quantityInContainersColumn.setCellValueFactory(new PropertyValueFactory<>("quantityInContainers"));

        productTableView.getColumns().addAll(productIdColumn, nameColumn, costColumn, unitColumn, quantityInContainersColumn);
        productTableView.getItems().addAll(controller.findProductsByShopId(shop.getId()));
        productTableView.setBorder(new Border(new BorderStroke(Color.BLACK, BorderStrokeStyle.SOLID, CornerRadii.EMPTY, BorderWidths.DEFAULT)));
        BorderPane.setMargin(productTableView, new Insets(5));
        productTableView.setPadding(new Insets(10, 10, 10, 10));

        return productTableView;
    }

    /**
     * @param shop - the store that is managed
     * @return - right component of the manager main view(borderPane) - which contains buttons
     */
    public Node buildRight(Shop shop) {
        Button addButton = buildAddButton(shop);

        Button deleteButton = buildDeleteButton();

        Button updateButton = buildUpdateButton(shop);

        Label editLabel = new Label("~~Edit~~");
        editLabel.setFont(Font.font("Arial", FontWeight.BOLD, 13));
        StackPane editPane = new StackPane(editLabel);

        VBox buttonsBox = new VBox(editPane, addButton, deleteButton, updateButton);
        buttonsBox.setSpacing(20);
        buttonsBox.setPadding(new Insets(20, 0, 0, 0));
        buttonsBox.setBackground(new Background(new BackgroundFill(Color.DARKGRAY, CornerRadii.EMPTY, Insets.EMPTY)));

        return buttonsBox;
    }

    private Button buildDeleteButton() {
        Button deleteButton = new Button("Delete");
        deleteButton.setOnAction(e -> {
            controller.removeProduct(productTableView.getSelectionModel().getSelectedItem());
            productTableView.getItems().remove(productTableView.getSelectionModel().getSelectedItem());
            productTableView.getSelectionModel().clearSelection();
        });
        deleteButton.setMinWidth(90);
        ButtonBeautification.beautifyTheButton(deleteButton);
        return deleteButton;
    }

    private Button buildUpdateButton(Shop shop) {
        Button updateButton = new Button("Update");
        updateButton.setOnAction(e -> {
            Stage buildProductRecordForUpdateView = buildProductForUpdate(productTableView.getSelectionModel().getSelectedItem(), shop);
            buildProductRecordForUpdateView.show();
        });
        updateButton.setMinWidth(90);
        ButtonBeautification.beautifyTheButton(updateButton);
        return updateButton;
    }

    private Button buildAddButton(Shop shop) {
        Button addButton = new Button("Add");
        addButton.setOnAction(e -> {
            Stage productRecordForAddView = buildProductForAdd(shop);
            productRecordForAddView.show();
        });
        addButton.setMinWidth(90);
        ButtonBeautification.beautifyTheButton(addButton);
        return addButton;
    }

    private Stage buildProductForAdd(Shop shop) {
        Stage stage = new Stage();
        stage.setTitle("Product record");

        Label nameLabel = new Label("Product name");
        nameField = new TextField();
        HBox nameBox = new HBox(nameLabel, nameField);
        nameBox.setSpacing(5);

        Label costLabel = new Label("Price");
        costField = new TextField();
        HBox costBox = new HBox(costLabel, costField);
        costBox.setSpacing(5);

        Label unitLabel = new Label("Unit");
        unitField = new TextField();
        HBox unitBox = new HBox(unitLabel, unitField);
        unitBox.setSpacing(5);

        Label quantityLabel = new Label("Quantity(in containers)");
        quantityField = new TextField();
        HBox quantityBox = new HBox(quantityLabel, quantityField);
        quantityBox.setSpacing(5);

        GridPane boxesGrid = new GridPane();
        Button finished = new Button("Finished");
        finished.setOnAction(e -> {
            if (!(nameField.getText().isBlank() && costField.getText().isBlank() && unitField.getText().isBlank() && quantityField.getText().isBlank())) {
                controller.insertProduct(nameField.getText(), Float.parseFloat(costField.getText()), unitField.getText(), Integer.parseInt(quantityField.getText()), shop.getId());

                List<Product> products = controller.findAllProducts();
                productTableView.getItems().add(products.get(products.size() - 1));
                stage.close();
            } else {
                Label errorLabel = new Label("Please complete all fields!");
                errorLabel.setTextFill(Color.RED);
                boxesGrid.add(errorLabel, 0, 5);
            }
        });
        ButtonBeautification.beautifyTheButton(finished);

        boxesGrid.add(nameLabel, 0, 0);
        boxesGrid.add(nameField, 1, 0);
        boxesGrid.add(costLabel, 0, 1);
        boxesGrid.add(costField, 1, 1);
        boxesGrid.add(unitLabel, 0, 2);
        boxesGrid.add(unitField, 1, 2);
        boxesGrid.add(quantityLabel, 0, 3);
        boxesGrid.add(quantityField, 1, 3);
        boxesGrid.add(finished, 1, 4);
        boxesGrid.setPadding(new Insets(20, 20, 20, 20));
        boxesGrid.setVgap(10);
        boxesGrid.setHgap(5);
        boxesGrid.setBackground(new Background(new BackgroundFill(Color.LIGHTGRAY, new CornerRadii(10), Insets.EMPTY)));

        stage.setScene(new Scene(boxesGrid, 340, 230));
        return stage;
    }

    private Stage buildProductForUpdate(Product product, Shop shop) {
        Stage stage = new Stage();
        stage.setTitle("Product record");

        Label nameLabel = new Label("Product name");
        nameField = new TextField();
        nameField.setText(product.getName());

        Label costLabel = new Label("Price");
        costField = new TextField();
        costField.setText(product.getCost() + "");

        Label unitLabel = new Label("Unit");
        unitField = new TextField();
        unitField.setText(product.getUnit());

        Label quantityLabel = new Label("Quantity(in containers)");
        quantityField = new TextField();
        quantityField.setText(product.getQuantityInContainers() + "");

        GridPane boxesGrid = new GridPane();
        Button finished = new Button("Finished");
        finished.setOnAction(e -> {
            if (!(nameField.getText().isBlank() && costField.getText().isBlank() && unitField.getText().isBlank() && quantityField.getText().isBlank())) {
                controller.updateProduct(product, nameField.getText(), Float.parseFloat(costField.getText()), unitField.getText(), Integer.parseInt(quantityField.getText()), shop.getId());
                productTableView.getItems().removeAll(controller.findProductsByShopId(shop.getId()));
                productTableView.getItems().addAll(controller.findProductsByShopId(shop.getId()));
                stage.close();
            } else {
                Label errorLabel = new Label("Please complete all fields!");
                errorLabel.setTextFill(Color.RED);
                boxesGrid.add(errorLabel, 0, 5);
            }
        });
        ButtonBeautification.beautifyTheButton(finished);

        boxesGrid.add(nameLabel, 0, 0);
        boxesGrid.add(nameField, 1, 0);
        boxesGrid.add(costLabel, 0, 1);
        boxesGrid.add(costField, 1, 1);
        boxesGrid.add(unitLabel, 0, 2);
        boxesGrid.add(unitField, 1, 2);
        boxesGrid.add(quantityLabel, 0, 3);
        boxesGrid.add(quantityField, 1, 3);
        boxesGrid.add(finished, 1, 4);
        boxesGrid.setPadding(new Insets(20, 20, 20, 20));
        boxesGrid.setVgap(10);
        boxesGrid.setHgap(5);
        boxesGrid.setBackground(new Background(new BackgroundFill(Color.LIGHTGRAY, new CornerRadii(10), Insets.EMPTY)));


        stage.setScene(new Scene(boxesGrid, 340, 230));
        return stage;
    }

    /**
     * @param user - logged user
     * @param shop - shop that is managed
     * @return - top component of manager main view - which contains home button
     * @throws FileNotFoundException -
     */
    public Node buildTop(User user, Shop shop) throws FileNotFoundException {
        Button homeButton = new Button();
        ImageView homeIcon = new ImageView(new Image(new FileInputStream("C:\\Users\\Emanuel Gadalean\\Links\\home.png")));
        homeIcon.setPreserveRatio(true);
        homeIcon.setFitWidth(15);
        homeButton.setGraphic(homeIcon);
        homeButton.setOnAction(e -> {
            try {
                Main.window.setScene(new Scene(new ManagerView(controller).display(user, shop), 700, 500));
            } catch (FileNotFoundException fileNotFoundException) {
                fileNotFoundException.printStackTrace();
            }
        });

        HBox buttonsBox = new HBox(homeButton);
        buttonsBox.setPadding(new Insets(5, 0, 5, 5));

        return buttonsBox;
    }

}
