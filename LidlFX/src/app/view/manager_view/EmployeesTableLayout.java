package app.view.manager_view;

import app.Main;
import app.controller.Controller;
import app.model.Shop;
import app.model.User;
import app.controller.security.Encryption;
import app.view.util.ButtonBeautification;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.List;

public class EmployeesTableLayout extends ManagerView {

    private TableView<User> employeeTableView;

    private TextField nameField;

    private TextField ageField;

    private TextField CNPField;

    private ComboBox<String> genderChoice;

    private TextField addressField;

    public EmployeesTableLayout(Controller controller) {
        super(controller);
    }

    public Node buildEmployeesTable(Shop shop) {
        employeeTableView = new TableView<>();

        TableColumn<User, Integer> employeeIdColumn = new TableColumn<>("Id");
        employeeIdColumn.setCellValueFactory(new PropertyValueFactory<>("id"));

        TableColumn<User, String> nameColumn = new TableColumn<>("Name");
        nameColumn.setCellValueFactory(new PropertyValueFactory<>("name"));

        TableColumn<User, Integer> ageColumn = new TableColumn<>("Age");
        ageColumn.setCellValueFactory(new PropertyValueFactory<>("age"));

        TableColumn<User, Long> CNPColumn = new TableColumn<>("CNP");
        CNPColumn.setCellValueFactory(new PropertyValueFactory<>("CNP"));

        TableColumn<User, String> genderColumn = new TableColumn<>("Gender");
        genderColumn.setCellValueFactory(new PropertyValueFactory<>("gender"));

        TableColumn<User, String> addressColumn = new TableColumn<>("Address");
        addressColumn.setCellValueFactory(new PropertyValueFactory<>("address"));

        employeeTableView.getColumns().addAll(employeeIdColumn, nameColumn, ageColumn, CNPColumn, genderColumn, addressColumn);
        employeeTableView.getItems().addAll(controller.findEmployeesByShopId(shop.getId()));
        employeeTableView.setBorder(new Border(new BorderStroke(Color.BLACK, BorderStrokeStyle.SOLID, CornerRadii.EMPTY, BorderWidths.DEFAULT)));
        BorderPane.setMargin(employeeTableView, new Insets(5));
        employeeTableView.setPadding(new Insets(10, 10, 10, 10));

        return employeeTableView;
    }

    /**
     * @param shop - the store that is managed
     * @return - right component of the manager main view(borderPane) - which contains buttons
     */
    public Node buildRight(Shop shop) {
        Button addButton = buildAddButton(shop);

        Button deleteButton = buildDeleteButton(shop);

        Button updateButton = buildUpdateButton(shop);

        Label editLabel = new Label("~~Edit~~");
        editLabel.setFont(Font.font("Arial", FontWeight.BOLD, 13));
        StackPane editPane = new StackPane(editLabel);

        VBox buttonsBox = new VBox(editPane, addButton, deleteButton, updateButton);
        buttonsBox.setSpacing(20);
        buttonsBox.setPadding(new Insets(20, 0, 0, 0));
        buttonsBox.setBackground(new Background(new BackgroundFill(Color.DARKGRAY, CornerRadii.EMPTY, Insets.EMPTY)));

        return buttonsBox;
    }

    private Button buildUpdateButton(Shop shop) {
        Button updateButton = new Button("Update");
        updateButton.setOnAction(e -> {
            Stage employeeRecordForUpdate = buildEmployeeRecordForUpdate(employeeTableView.getSelectionModel().getSelectedItem(), shop);
            employeeRecordForUpdate.show();
        });
        updateButton.setMinWidth(90);
        ButtonBeautification.beautifyTheButton(updateButton);
        return updateButton;
    }

    private Button buildDeleteButton(Shop shop) {
        Button deleteButton = new Button("Delete");
        deleteButton.setOnAction(e -> {
            controller.removeEmployee(employeeTableView.getSelectionModel().getSelectedItem(), shop);
            employeeTableView.getItems().remove(employeeTableView.getSelectionModel().getSelectedItem());
            employeeTableView.getSelectionModel().clearSelection();
        });
        deleteButton.setMinWidth(90);
        ButtonBeautification.beautifyTheButton(deleteButton);
        return deleteButton;
    }

    private Button buildAddButton(Shop shop) {
        Button addButton = new Button("Add");
        addButton.setOnAction(e -> {
            Stage employeeRecordForAddView = buildEmployeeRecordForAdd(shop);
            employeeRecordForAddView.show();
        });
        ButtonBeautification.beautifyTheButton(addButton);
        addButton.setMinWidth(90);
        return addButton;
    }

    private Stage buildEmployeeRecordForAdd(Shop shop) {
        Stage employeeRecord = new Stage();
        employeeRecord.setTitle("Employee record");

        Label employeeLabel = new Label("Personal Data");
        employeeLabel.setFont(Font.font("Arial", FontWeight.BOLD, 17));
        StackPane title = new StackPane(employeeLabel);

        Label nameLabel = new Label("Name:");
        nameField = new TextField();
        HBox nameBox = new HBox(nameLabel, nameField);
        nameBox.setSpacing(12);

        Label ageLabel = new Label("Age:");
        ageField = new TextField();
        HBox ageBox = new HBox(ageLabel, ageField);
        ageBox.setSpacing(12);

        Label CNPLabel = new Label("CNP:");
        CNPField = new TextField();
        HBox CNPBox = new HBox(CNPLabel, CNPField);
        CNPBox.setSpacing(12);

        Label genderLabel = new Label("Gender:");
        genderChoice = new ComboBox<>();
        genderChoice.getItems().addAll("Masculin", "Feminin", "Altele");
        HBox genderBox = new HBox(genderLabel, genderChoice);
        genderBox.setSpacing(12);

        Label addressLabel = new Label("Address:");
        addressField = new TextField();
        HBox addressBox = new HBox(addressLabel, addressField);
        addressBox.setSpacing(12);

        Label employeeAccountLabel = new Label("Account Data");
        employeeAccountLabel.setFont(Font.font("Arial", FontWeight.BOLD, 17));
        StackPane employeeAccountTitle = new StackPane(employeeAccountLabel);

        Label usernameLabel = new Label("Username:");
        TextField usernameField = new TextField();
        HBox usernameBox = new HBox(usernameLabel, usernameField);
        usernameBox.setSpacing(10);

        Label passwordLabel = new Label("Password:");
        TextField passwordField = new TextField();
        HBox passwordBox = new HBox(passwordLabel, passwordField);
        passwordBox.setSpacing(10);

        GridPane employeeGrid = new GridPane();

        Button addButton = new Button("Finished");
        addButton.setOnAction(e -> {
            if (nameField.getText() != null && ageField.getText() != null && CNPField.getText() != null && addressField.getText() != null && genderChoice.getValue() != null) {
                controller.insertEmployee(nameField.getText(), Integer.parseInt(ageField.getText()), Long.parseLong(CNPField.getText()), genderChoice.getValue(), addressField.getText(), usernameField.getText(), passwordField.getText(), shop);

                List<User> employees = controller.findEmployeesByShopId(shop.getId());
                employeeTableView.getItems().add(employees.get(employees.size() - 1));
                employeeRecord.close();
            } else {
                Label errorLabel = new Label("Please complete all fields!");
                errorLabel.setTextFill(Color.RED);
                employeeGrid.add(errorLabel, 0, 9);
            }
        });
        ButtonBeautification.beautifyTheButton(addButton);
        HBox buttonBox = new HBox(addButton);
        buttonBox.setPadding(new Insets(0, 0, 0, 150));


        employeeGrid.add(title, 0, 1);
        employeeGrid.add(employeeAccountTitle, 7, 1);
        employeeGrid.add(nameBox, 0, 2);
        employeeGrid.add(ageBox, 0, 3);
        employeeGrid.add(CNPBox, 0, 4);
        employeeGrid.add(genderBox, 0, 5);
        employeeGrid.add(addressBox, 0, 6);
        employeeGrid.add(usernameBox, 7, 2);
        employeeGrid.add(passwordBox, 7, 3);
        employeeGrid.add(buttonBox, 7, 4);
        employeeGrid.setVgap(18);
        employeeGrid.setHgap(10);
        employeeGrid.setPadding(new Insets(20, 20, 20, 20));
        employeeGrid.setBackground(new Background(new BackgroundFill(Color.LIGHTGRAY, new CornerRadii(10), Insets.EMPTY)));

        employeeRecord.setScene(new Scene(employeeGrid, 600, 350));
        return employeeRecord;
    }

    private Stage buildEmployeeRecordForUpdate(User employee, Shop shop) {
        Stage employeeRecord = new Stage();
        employeeRecord.setTitle("Employee record");

        Label employeeLabel = new Label("Personal Data");
        employeeLabel.setFont(Font.font("Arial", FontWeight.BOLD, 17));
        StackPane title = new StackPane(employeeLabel);

        Label nameLabel = new Label("Name");
        nameField = new TextField();
        nameField.setText(employee.getName());
        HBox nameBox = new HBox(nameLabel, nameField);
        nameBox.setSpacing(5);

        Label ageLabel = new Label("Age");
        ageField = new TextField();
        ageField.setText(employee.getAge() + "");
        HBox ageBox = new HBox(ageLabel, ageField);
        ageBox.setSpacing(5);

        Label CNPLabel = new Label("CNP");
        CNPField = new TextField();
        CNPField.setText(employee.getCNP() + "");
        HBox CNPBox = new HBox(CNPLabel, CNPField);
        CNPBox.setSpacing(5);

        Label genderLabel = new Label("Gender");
        genderChoice = new ComboBox<>();
        genderChoice.getItems().addAll("Masculin", "Feminin", "Altele");
        genderChoice.setValue(employee.getGender());
        HBox genderBox = new HBox(genderLabel, genderChoice);
        genderBox.setSpacing(5);

        Label addressLabel = new Label("Address");
        addressField = new TextField();
        addressField.setText(employee.getAddress());
        HBox addressBox = new HBox(addressLabel, addressField);
        addressBox.setSpacing(5);

        Label employeeAccountLabel = new Label("Account Data");
        employeeAccountLabel.setFont(Font.font("Arial", FontWeight.BOLD, 17));
        StackPane employeeAccountTitle = new StackPane(employeeAccountLabel);

        Label usernameLabel = new Label("Username:");
        TextField usernameField = new TextField();
        usernameField.setText(employee.getUsername());
        HBox usernameBox = new HBox(usernameLabel, usernameField);
        usernameBox.setSpacing(10);

        Label passwordLabel = new Label("Password:");
        TextField passwordField = new TextField();
        passwordField.setText(Encryption.decryption(employee.getPassword()));
        HBox passwordBox = new HBox(passwordLabel, passwordField);
        passwordBox.setSpacing(10);

        GridPane employeeGrid = new GridPane();
        Button addButton = new Button("Finished");
        addButton.setOnAction(e -> {
            if (nameField.getText() != null && ageField.getText() != null && CNPField.getText() != null && addressField.getText() != null && genderChoice.getValue() != null) {
                controller.updateEmployee(employee, nameField.getText(), Integer.parseInt(ageField.getText()), Long.parseLong(CNPField.getText()), genderChoice.getValue(), addressField.getText(), usernameField.getText(), passwordField.getText(), shop);
                employeeTableView.getItems().removeAll(controller.findEmployeesByShopId(shop.getId()));
                employeeTableView.getItems().addAll(controller.findEmployeesByShopId(shop.getId()));
                employeeRecord.close();
                employeeTableView.getSelectionModel().clearSelection();
            } else {
                Label errorLabel = new Label("Please complete all fields!");
                errorLabel.setTextFill(Color.RED);
                employeeGrid.add(errorLabel, 0, 9);
            }
        });
        addButton.setBorder(new Border(new BorderStroke(Color.BLACK, BorderStrokeStyle.SOLID, new CornerRadii(5), BorderWidths.DEFAULT)));
        addButton.setBackground(new Background(new BackgroundFill(Color.MIDNIGHTBLUE, new CornerRadii(10), Insets.EMPTY)));
        addButton.setTextFill(Color.WHITE);
        HBox buttonBox = new HBox(addButton);
        buttonBox.setPadding(new Insets(0, 0, 0, 150));

        employeeGrid.add(title, 0, 1);
        employeeGrid.add(employeeAccountTitle, 7, 1);
        employeeGrid.add(nameBox, 0, 2);
        employeeGrid.add(ageBox, 0, 3);
        employeeGrid.add(CNPBox, 0, 4);
        employeeGrid.add(genderBox, 0, 5);
        employeeGrid.add(addressBox, 0, 6);
        employeeGrid.add(usernameBox, 7, 2);
        employeeGrid.add(passwordBox, 7, 3);
        employeeGrid.add(buttonBox, 7, 4);
        employeeGrid.setVgap(18);
        employeeGrid.setHgap(10);
        employeeGrid.setPadding(new Insets(20, 20, 20, 20));
        employeeGrid.setBackground(new Background(new BackgroundFill(Color.LIGHTGRAY, new CornerRadii(10), Insets.EMPTY)));

        employeeRecord.setScene(new Scene(employeeGrid, 600, 350));
        return employeeRecord;
    }

    /**
     * @param user - logged user
     * @param shop - shop that is managed
     * @return - top component of manager main view - which contains home button
     * @throws FileNotFoundException -
     */
    public Node buildTop(User user, Shop shop) throws FileNotFoundException {
        Button homeButton = new Button();
        ImageView homeIcon = new ImageView(new Image(new FileInputStream("C:\\Users\\Emanuel Gadalean\\Links\\home.png")));
        homeIcon.setPreserveRatio(true);
        homeIcon.setFitWidth(15);
        homeButton.setGraphic(homeIcon);
        homeButton.setOnAction(e -> {
            try {
                Main.window.setScene(new Scene(new ManagerView(controller).display(user, shop), 700, 500));
            } catch (FileNotFoundException fileNotFoundException) {
                fileNotFoundException.printStackTrace();
            }
        });

        HBox buttonsBox = new HBox(homeButton);
        buttonsBox.setPadding(new Insets(5, 0, 5, 5));

        return buttonsBox;
    }

}
