package app.view.manager_view;

import app.Main;
import app.controller.Controller;
import app.model.Shop;
import app.model.User;
import app.view.LoginView;
import app.view.util.ButtonBeautification;
import app.view.util.SponsorIcons;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;

import java.io.FileNotFoundException;

public class ManagerView {

    Controller controller;

    public BorderPane managerLayout;

    public ManagerView(Controller controller) {
        this.controller = controller;
    }

    /**
     * @param user - logged in user
     * @param shop - the store that the user manages
     * @return - main view for user which is a borderPane
     * @throws FileNotFoundException -
     */
    public Parent display(User user, Shop shop) throws FileNotFoundException {
        Main.window.setTitle("Lidl-Manager");

        managerLayout = new BorderPane();

        managerLayout.setTop(buildTopLayout());
        managerLayout.setCenter(buildCenter(user, shop));
        managerLayout.setRight(SponsorIcons.build());

        managerLayout.setBackground(new Background(new BackgroundFill(Color.DARKGRAY, CornerRadii.EMPTY, Insets.EMPTY)));
        return managerLayout;
    }

    /**
     * build top component of the main view(borderPane)
     *
     * @return - a menu bar
     */
    private Node buildTopLayout() {
        MenuItem logOut = new MenuItem("Log out");
        logOut.setOnAction(e -> Main.window.setScene(new Scene(new LoginView(controller).display(), 260, 300)));

        Menu fileMenu = new Menu("File");
        MenuItem exit = new MenuItem("Exit");
        exit.setOnAction(e -> Main.window.close());
        fileMenu.getItems().addAll(logOut, exit);

        return new MenuBar(fileMenu);
    }

    /**
     * @param user - logged in user
     * @param shop - the store that the user manages
     * @return - center component of the main view(borderPane)
     */
    private Node buildCenter(User user, Shop shop) {
        Button employees = buildEmployeesButton(user, shop);

        Button products = buildProductsButton(user, shop);

        Button costs = buildCostsButton(user, shop);

        Button encashments = buildEncashmentsButton(user, shop);

        Label goTo = new Label("Go to:");
        goTo.setFont(Font.font("Impact", FontWeight.BOLD, 20));

        VBox buttonsBox = new VBox(goTo, employees, products, costs, encashments);
        buttonsBox.setSpacing(5);

        VBox userDataBox = buildLoggedUserData(user, shop);

        VBox boxForAllComponents = new VBox(userDataBox, buttonsBox);
        boxForAllComponents.setSpacing(40);
        boxForAllComponents.setPadding(new Insets(10, 10, 10, 10));
        BorderPane.setMargin(boxForAllComponents, new Insets(0, 350, 0, 0));

        return boxForAllComponents;
    }

    private Button buildEmployeesButton(User user, Shop shop) {
        Button employees = new Button("Employees");
        employees.setOnAction(e -> {
            EmployeesTableLayout employeesLayout = new EmployeesTableLayout(controller);

            managerLayout.setCenter(employeesLayout.buildEmployeesTable(shop));
            managerLayout.setRight(employeesLayout.buildRight(shop));
            try {
                managerLayout.setTop(employeesLayout.buildTop(user, shop));
            } catch (FileNotFoundException fileNotFoundException) {
                fileNotFoundException.printStackTrace();
            }
        });
        ButtonBeautification.beautifyTheButton(employees);
        employees.setMinWidth(130);
        return employees;
    }

    private Button buildProductsButton(User user, Shop shop) {
        Button products = new Button("Products");
        products.setOnAction(e -> {
            ProductsTableLayout productsTableLayout = new ProductsTableLayout(controller);

            managerLayout.setCenter(productsTableLayout.buildProductsTable(shop));
            managerLayout.setRight(productsTableLayout.buildRight(shop));
            try {
                managerLayout.setTop(productsTableLayout.buildTop(user, shop));
            } catch (FileNotFoundException fileNotFoundException) {
                fileNotFoundException.printStackTrace();
            }
        });
        ButtonBeautification.beautifyTheButton(products);
        products.setMinWidth(130);
        return products;
    }

    private Button buildCostsButton(User user, Shop shop) {
        Button costs = new Button("Costs");
        costs.setOnAction(e -> {
            CostsTableLayout costsTableLayout = new CostsTableLayout(controller);

            managerLayout.setCenter(costsTableLayout.buildExpenseTable(shop));
            managerLayout.setRight(costsTableLayout.buildRight(shop));
            try {
                managerLayout.setTop(costsTableLayout.buildTop(user, shop));
            } catch (FileNotFoundException fileNotFoundException) {
                fileNotFoundException.printStackTrace();
            }

        });
        ButtonBeautification.beautifyTheButton(costs);
        costs.setMinWidth(130);
        return costs;
    }

    private Button buildEncashmentsButton(User user, Shop shop) {
        Button encashments = new Button("Encashments");
        encashments.setOnAction(e -> {
            EncashmentsTableLayout encashmentsTableLayout = new EncashmentsTableLayout(controller);

            managerLayout.setCenter(encashmentsTableLayout.buildEncashmentTable(shop));
            managerLayout.setRight(encashmentsTableLayout.buildRight(shop));
            try {
                managerLayout.setTop(encashmentsTableLayout.buildTop(user, shop));
            } catch (FileNotFoundException fileNotFoundException) {
                fileNotFoundException.printStackTrace();
            }
        });
        ButtonBeautification.beautifyTheButton(encashments);
        encashments.setMinWidth(130);
        return encashments;
    }

    private VBox buildLoggedUserData(User user, Shop shop) {
        Label userRole = new Label("User Role:");
        userRole.setFont(Font.font("Arial", FontWeight.BOLD, 12));
        Label userRoleLabel = new Label("Manager");
        HBox userRoleBox = new HBox(userRole, userRoleLabel);
        userRoleBox.setSpacing(5);

        Label userStatusLabel = new Label("Status:");
        userStatusLabel.setFont(Font.font("Arial", FontWeight.BOLD, 12));
        Label userStatusL = new Label(user.getUserStatus());
        HBox userStatusBox = new HBox(userStatusLabel, userStatusL);
        userStatusBox.setSpacing(5);

        Label usernameLabel = new Label("Username:");
        usernameLabel.setFont(Font.font("Arial", FontWeight.BOLD, 12));
        Label usernameInfo = new Label(user.getUsername());
        HBox usernameBox = new HBox(usernameLabel, usernameInfo);
        usernameBox.setSpacing(5);

        Label numOfEmployees = new Label("Number of employees:");
        numOfEmployees.setFont(Font.font("Arial", FontWeight.BOLD, 12));
        Label numOfEmployeesInfo = new Label(controller.findEmployeesByShopId(shop.getId()).size() + "");
        HBox numOfEmployeesBox = new HBox(numOfEmployees, numOfEmployeesInfo);
        numOfEmployeesBox.setSpacing(5);

        VBox userDataBox = new VBox(usernameBox, userStatusBox, userRoleBox, numOfEmployeesBox);
        userDataBox.setSpacing(10);
        userDataBox.setBackground(new Background(new BackgroundFill(Color.LIGHTBLUE, new CornerRadii(5), Insets.EMPTY)));
        userDataBox.setPadding(new Insets(5, 5, 5, 5));
        return userDataBox;
    }

}
