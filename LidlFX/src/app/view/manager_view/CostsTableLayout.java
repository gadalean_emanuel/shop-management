package app.view.manager_view;

import app.Main;
import app.model.User;
import app.controller.Controller;
import app.model.Expense;
import app.model.Shop;
import app.view.util.ButtonBeautification;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.time.LocalDate;
import java.util.List;

public class CostsTableLayout extends ManagerView {

    private TableView<Expense> expenseTableView;

    public CostsTableLayout(Controller controller) {
        super(controller);
    }


    public Node buildExpenseTable(Shop shop) {
        expenseTableView = new TableView<>();

        TableColumn<Expense, String> nameColumn = new TableColumn<>("Expense Name");
        nameColumn.setCellValueFactory(new PropertyValueFactory<>("name"));
        nameColumn.setMinWidth(200);

        TableColumn<Expense, Double> costValueColumn = new TableColumn<>("Expense Value(LEI)");
        costValueColumn.setCellValueFactory(new PropertyValueFactory<>("cost"));
        costValueColumn.setMinWidth(155);

        TableColumn<Expense, LocalDate> dateTableColumn = new TableColumn<>("Date");
        dateTableColumn.setCellValueFactory(new PropertyValueFactory<>("date"));

        expenseTableView.getColumns().addAll(nameColumn, costValueColumn, dateTableColumn);
        expenseTableView.getItems().addAll(controller.findAllExpensesByShopId(shop.getId()));
        expenseTableView.setBorder(new Border(new BorderStroke(Color.BLACK, BorderStrokeStyle.SOLID, CornerRadii.EMPTY, BorderWidths.DEFAULT)));
        BorderPane.setMargin(expenseTableView, new Insets(5));
        expenseTableView.setPadding(new Insets(10, 10, 10, 10));

        return expenseTableView;
    }

    /**
     * @param shop - the store that is managed
     * @return - right component of the manager main view(borderPane) - which contains buttons
     */
    public Node buildRight(Shop shop) {
        Button addButton = buildAddButton(shop);

        Button deleteButton = buildDeleteButton();

        Button updateButton = buildUpdateButton(shop);

        Label editLabel = new Label("~~Edit~~");
        editLabel.setFont(Font.font("Arial", FontWeight.BOLD, 13));
        StackPane editPane = new StackPane(editLabel);

        VBox buttonsBox = new VBox(editPane, addButton, deleteButton, updateButton);
        buttonsBox.setSpacing(20);
        buttonsBox.setPadding(new Insets(20, 0, 0, 0));
        buttonsBox.setBackground(new Background(new BackgroundFill(Color.DARKGRAY, CornerRadii.EMPTY, Insets.EMPTY)));

        return buttonsBox;
    }

    private Button buildUpdateButton(Shop shop) {
        Button updateButton = new Button("Update");
        updateButton.setOnAction(e -> {
            Stage costRecordForUpdateView = buildCostRecordForUpdate(expenseTableView.getSelectionModel().getSelectedItem(), shop);
            costRecordForUpdateView.show();
        });
        updateButton.setMinWidth(90);
        ButtonBeautification.beautifyTheButton(updateButton);
        return updateButton;
    }

    private Button buildDeleteButton() {
        Button deleteButton = new Button("Delete");
        deleteButton.setOnAction(e -> {
            controller.removeExpense(expenseTableView.getSelectionModel().getSelectedItem());
            expenseTableView.getItems().remove(expenseTableView.getSelectionModel().getSelectedItem());
            expenseTableView.getSelectionModel().clearSelection();
        });
        deleteButton.setMinWidth(90);
        ButtonBeautification.beautifyTheButton(deleteButton);
        return deleteButton;
    }

    private Button buildAddButton(Shop shop) {
        Button addButton = new Button("Add");
        addButton.setOnAction(e -> {
            Stage costRecordForAddView = buildCostRecordForAdd(shop);
            costRecordForAddView.show();
        });
        addButton.setMinWidth(90);
        ButtonBeautification.beautifyTheButton(addButton);
        return addButton;
    }

    private Stage buildCostRecordForAdd(Shop shop) {
        Stage stage = new Stage();
        stage.setTitle("Cost record");

        Label nameLabel = new Label("Expense name:");
        TextField nameField = new TextField();
        HBox nameBox = new HBox(nameLabel, nameField);
        nameBox.setSpacing(5);

        Label costLabel = new Label("Expense value(LEI):");
        TextField costField = new TextField();
        HBox costBox = new HBox(costLabel, costField);
        costBox.setSpacing(5);

        Label dateLabel = new Label("Date:");
        DatePicker dateField = new DatePicker();
        HBox unitBox = new HBox(dateLabel, dateField);
        unitBox.setSpacing(5);

        GridPane boxesGrid = new GridPane();
        Button finished = new Button("Finished");
        finished.setOnAction(e -> {
            if (!(nameField.getText().isBlank() && costField.getText().isBlank() && dateField.getValue() == null)) {
                controller.insertExpense(nameField.getText(), Float.parseFloat(costField.getText()), dateField.getValue(), shop.getId());
                List<Expense> expenses = controller.findAllExpenses();
                expenseTableView.getItems().add(expenses.get(expenses.size() - 1));
                stage.close();
            } else {
                Label errorLabel = new Label("Please complete all fields!");
                errorLabel.setTextFill(Color.RED);
                boxesGrid.add(errorLabel, 0, 5, 3, 1);
            }
        });
        ButtonBeautification.beautifyTheButton(finished);

        boxesGrid.add(nameLabel, 0, 0);
        boxesGrid.add(nameField, 1, 0);
        boxesGrid.add(costLabel, 0, 1);
        boxesGrid.add(costField, 1, 1);
        boxesGrid.add(dateLabel, 0, 2);
        boxesGrid.add(dateField, 1, 2);
        boxesGrid.add(finished, 1, 3);
        boxesGrid.setPadding(new Insets(20, 20, 20, 20));
        boxesGrid.setVgap(10);
        boxesGrid.setHgap(5);
        boxesGrid.setBackground(new Background(new BackgroundFill(Color.LIGHTGRAY, new CornerRadii(10), Insets.EMPTY)));

        stage.setScene(new Scene(boxesGrid, 320, 220));
        return stage;
    }

    private Stage buildCostRecordForUpdate(Expense expense, Shop shop) {
        Stage stage = new Stage();
        stage.setTitle("Cost record");

        Label nameLabel = new Label("Expense name:");
        TextField nameField = new TextField();
        nameField.setText(expense.getName());
        HBox nameBox = new HBox(nameLabel, nameField);
        nameBox.setSpacing(5);

        Label costLabel = new Label("Expense value(LEI):");
        TextField costField = new TextField();
        costField.setText(expense.getCost() + "");
        HBox costBox = new HBox(costLabel, costField);
        costBox.setSpacing(5);

        Label dateLabel = new Label("Date:");
        DatePicker dateField = new DatePicker();
        dateField.setValue(expense.getDate());
        HBox unitBox = new HBox(dateLabel, dateField);
        unitBox.setSpacing(5);

        GridPane boxesGrid = new GridPane();
        Button finished = new Button("Finished");
        finished.setOnAction(e -> {
            if (!(nameField.getText().isBlank() && costField.getText().isBlank() && dateField.getValue() == null)) {
                controller.updateExpense(expense, nameField.getText(), Float.parseFloat(costField.getText()), dateField.getValue());
                expenseTableView.getItems().removeAll(controller.findAllExpensesByShopId(shop.getId()));
                expenseTableView.getItems().addAll((controller.findAllExpensesByShopId(shop.getId())));
                stage.close();
                expenseTableView.getSelectionModel().clearSelection();
            } else {
                Label errorLabel = new Label("Please complete all fields!");
                errorLabel.setTextFill(Color.RED);
                boxesGrid.add(errorLabel, 0, 5, 3, 1);
            }
        });
        ButtonBeautification.beautifyTheButton(finished);

        boxesGrid.add(nameLabel, 0, 0);
        boxesGrid.add(nameField, 1, 0);
        boxesGrid.add(costLabel, 0, 1);
        boxesGrid.add(costField, 1, 1);
        boxesGrid.add(dateLabel, 0, 2);
        boxesGrid.add(dateField, 1, 2);
        boxesGrid.add(finished, 1, 3);
        boxesGrid.setPadding(new Insets(20, 20, 20, 20));
        boxesGrid.setVgap(10);
        boxesGrid.setHgap(5);
        boxesGrid.setBackground(new Background(new BackgroundFill(Color.LIGHTGRAY, new CornerRadii(10), Insets.EMPTY)));

        stage.setScene(new Scene(boxesGrid, 320, 220));
        return stage;
    }

    /**
     * @param user - logged user
     * @param shop - shop that is managed
     * @return - top component of manager main view - which contains home button
     * @throws FileNotFoundException -
     */
    public Node buildTop(User user, Shop shop) throws FileNotFoundException {
        Button homeButton = new Button();
        ImageView homeIcon = new ImageView(new Image(new FileInputStream("C:\\Users\\Emanuel Gadalean\\Links\\home.png")));
        homeIcon.setPreserveRatio(true);
        homeIcon.setFitWidth(15);
        homeButton.setGraphic(homeIcon);
        homeButton.setOnAction(e -> {
            try {
                Main.window.setScene(new Scene(new ManagerView(controller).display(user, shop), 700, 500));
            } catch (FileNotFoundException fileNotFoundException) {
                fileNotFoundException.printStackTrace();
            }
        });

        HBox buttonsBox = new HBox(homeButton);
        buttonsBox.setPadding(new Insets(5, 0, 5, 5));

        return buttonsBox;
    }
}


