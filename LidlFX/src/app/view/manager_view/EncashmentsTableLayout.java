package app.view.manager_view;

import app.Main;
import app.controller.Controller;
import app.model.Encashment;
import app.model.Product;
import app.model.Shop;
import app.model.User;
import app.view.util.ButtonBeautification;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.time.LocalDate;

public class EncashmentsTableLayout extends ManagerView {


    private TableView<Encashment> encashmentTableView;

    public EncashmentsTableLayout(Controller controller) {
        super(controller);
    }

    public Node buildEncashmentTable(Shop shop) {
        encashmentTableView = new TableView<>();

        TableColumn<Encashment, String> productNameColumn = new TableColumn<>("Product");
        productNameColumn.setCellValueFactory(new PropertyValueFactory<>("productName"));
        productNameColumn.setMinWidth(200);

        TableColumn<Encashment, Integer> numOfProducts = new TableColumn<>("Num of products");
        numOfProducts.setCellValueFactory(new PropertyValueFactory<>("numOfProducts"));
        numOfProducts.setMinWidth(50);

        TableColumn<Encashment, Double> costValueColumn = new TableColumn<>("Value(LEI)");
        costValueColumn.setCellValueFactory(new PropertyValueFactory<>("value"));
        costValueColumn.setMinWidth(155);

        TableColumn<Encashment, LocalDate> dateTableColumn = new TableColumn<>("Date");
        dateTableColumn.setCellValueFactory(new PropertyValueFactory<>("date"));

        encashmentTableView.getColumns().addAll(productNameColumn, numOfProducts, costValueColumn, dateTableColumn);
        encashmentTableView.getItems().addAll(controller.findAllEncashmentsByShopId(shop.getId()));
        encashmentTableView.setBorder(new Border(new BorderStroke(Color.BLACK, BorderStrokeStyle.SOLID, CornerRadii.EMPTY, BorderWidths.DEFAULT)));
        BorderPane.setMargin(encashmentTableView, new Insets(5));
        encashmentTableView.setPadding(new Insets(10, 10, 10, 10));

        return encashmentTableView;
    }

    /**
     * @param shop - the store that is managed
     * @return - right component of the manager main view(borderPane) - which contains buttons
     */
    public Node buildRight(Shop shop) {
        Button deleteButton = buildDeleteButton();

        Button updateButton = buildUpdateButton(shop);

        Button depositButton = buildDepositButton(shop);

        Label editLabel = new Label("~~Edit~~");
        editLabel.setFont(Font.font("Arial", FontWeight.BOLD, 13));
        StackPane editPane = new StackPane(editLabel);

        VBox buttonsBox = new VBox(editPane, deleteButton, updateButton, depositButton);
        buttonsBox.setSpacing(20);
        buttonsBox.setPadding(new Insets(20, 0, 0, 0));
        buttonsBox.setBackground(new Background(new BackgroundFill(Color.DARKGRAY, CornerRadii.EMPTY, Insets.EMPTY)));

        return buttonsBox;
    }

    private Button buildDepositButton(Shop shop) {
        Button depositButton = new Button("Deposit");
        depositButton.setOnAction(e -> {
            try {
                Stage shopDepositView = buildShopDepositLayout(shop);
                shopDepositView.show();
            } catch (FileNotFoundException fileNotFoundException) {
                fileNotFoundException.printStackTrace();
            }
        });
        depositButton.setMinWidth(65);
        ButtonBeautification.beautifyTheButton(depositButton);
        return depositButton;
    }

    private Button buildUpdateButton(Shop shop) {
        Button updateButton = new Button("Update");
        updateButton.setOnAction(e -> {
            try {
                Stage encashmentRecordForUpdateView = buildEncashmentRecordForUpdate(encashmentTableView.getSelectionModel().getSelectedItem(), shop);
                encashmentRecordForUpdateView.show();
            } catch (FileNotFoundException fileNotFoundException) {
                fileNotFoundException.printStackTrace();
            }
        });
        updateButton.setMinWidth(90);
        ButtonBeautification.beautifyTheButton(updateButton);
        return updateButton;
    }

    private Button buildDeleteButton() {
        Button deleteButton = new Button("Delete");
        deleteButton.setOnAction(e -> {
            controller.removeEncashment(encashmentTableView.getSelectionModel().getSelectedItem());
            encashmentTableView.getItems().remove(encashmentTableView.getSelectionModel().getSelectedItem());
            encashmentTableView.getSelectionModel().clearSelection();
        });
        deleteButton.setMinWidth(90);
        ButtonBeautification.beautifyTheButton(deleteButton);
        return deleteButton;
    }

    private Stage buildEncashmentRecordForUpdate(Encashment encashment, Shop shop) throws FileNotFoundException {
        Stage stage = new Stage();
        stage.setTitle("Encashemnt edit");

        Label nameLabel = new Label("Product");
        TextField nameField = new TextField();
        nameField.setText(encashment.getProductName());
        nameField.setEditable(false);

        Label costLabel = new Label("Encashment value(LEI):");
        TextField costField = new TextField();
        costField.setEditable(false);
        costField.setText(encashment.getValue() + "");

        Label numOfProducts = new Label("Num of products");
        TextField numOfProdField = new TextField();
        numOfProdField.setText(encashment.getNumOfProducts() + "");
        numOfProdField.textProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue != null && !newValue.isEmpty()) {
                int value = Integer.parseInt(newValue);
                costField.setText(controller.findProductByShopId(shop.getId(), nameField.getText()).getCost() * value + "");
            } else {
                costField.setText(0 + "");
            }
        });

        Label dateLabel = new Label("Date");
        DatePicker dateField = new DatePicker();
        dateField.setValue(encashment.getDate());
        dateField.setEditable(false);

        GridPane boxesGrid = new GridPane();
        Button finished = new Button("Finished");
        finished.setOnAction(e -> {
            int newNumOfProdValue = Integer.parseInt(numOfProdField.getText());
            int oldNumOfProdValue = encashment.getNumOfProducts();
            controller.updateDeposit(encashment.getProductName(), oldNumOfProdValue - newNumOfProdValue, shop.getId());
            controller.updateEncashment(encashment, numOfProdField.getText(), costField.getText());
            encashmentTableView.getItems().removeAll(controller.findAllEncashmentsByShopId(shop.getId()));
            encashmentTableView.getItems().addAll(controller.findAllEncashmentsByShopId(shop.getId()));
            stage.close();
            encashmentTableView.getSelectionModel().clearSelection();
        });
        ButtonBeautification.beautifyTheButton(finished);

        boxesGrid.add(nameLabel, 0, 0);
        boxesGrid.add(nameField, 1, 0);
        boxesGrid.add(numOfProducts, 0, 1);
        boxesGrid.add(numOfProdField, 1, 1);
        boxesGrid.add(costLabel, 0, 2);
        boxesGrid.add(costField, 1, 2);
        boxesGrid.add(dateLabel, 0, 3);
        boxesGrid.add(dateField, 1, 3);
        boxesGrid.add(finished, 1, 4);
        boxesGrid.setPadding(new Insets(20, 20, 20, 20));
        boxesGrid.setVgap(10);
        boxesGrid.setHgap(5);

        boxesGrid.setBackground(new Background(new BackgroundFill(Color.LIGHTGRAY, new CornerRadii(10), Insets.EMPTY)));

        stage.setScene(new Scene(boxesGrid, 360, 220));
        return stage;
    }


    /**
     * @param shop -shop that is managed
     * @return - shop deposit view witch contains a table with 2 columns (name of the product + quantity of that product available in shop)
     * @throws FileNotFoundException -
     */
    private Stage buildShopDepositLayout(Shop shop) throws FileNotFoundException {
        Stage stage = new Stage();
        stage.setTitle("Deposit");
        stage.getIcons().add(new Image(new FileInputStream("C:\\Users\\Emanuel Gadalean\\Links\\depo.png")));

        TableView<Product> products = new TableView<>();

        TableColumn<Product, String> productName = new TableColumn<>("Product");
        productName.setCellValueFactory(new PropertyValueFactory<>("name"));

        TableColumn<Product, Integer> productQuantity = new TableColumn<>("Quantity");
        productQuantity.setCellValueFactory(new PropertyValueFactory<>("quantityInContainers"));
        productQuantity.setMinWidth(100);

        products.getColumns().addAll(productName, productQuantity);
        products.getItems().addAll(controller.findProductsByShopId(shop.getId()));

        StackPane stackPane = new StackPane(products);
        stackPane.setPadding(new Insets(20, 20, 20, 20));

        stage.setScene(new Scene(stackPane, 260, 400));
        return stage;
    }


    /**
     * @param user - logged user
     * @param shop - shop that is managed
     * @return - top component of manager main view - which contains home button
     * @throws FileNotFoundException -
     */
    public Node buildTop(User user, Shop shop) throws FileNotFoundException {
        Button homeButton = new Button();
        ImageView homeIcon = new ImageView(new Image(new FileInputStream("C:\\Users\\Emanuel Gadalean\\Links\\home.png")));
        homeIcon.setPreserveRatio(true);
        homeIcon.setFitWidth(15);
        homeButton.setGraphic(homeIcon);
        homeButton.setOnAction(e -> {
            try {
                Main.window.setScene(new Scene(new ManagerView(controller).display(user, shop), 700, 500));
            } catch (FileNotFoundException fileNotFoundException) {
                fileNotFoundException.printStackTrace();
            }
        });

        HBox buttonsBox = new HBox(homeButton);
        buttonsBox.setPadding(new Insets(5, 0, 5, 5));

        return buttonsBox;
    }

}
