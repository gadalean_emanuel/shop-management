package app.view;

import app.Main;
import app.controller.Controller;
import app.model.Shop;
import app.model.User;
import app.repository.constants.UserRoles;
import app.view.admin_view.AdminView;
import app.view.employee_view.EmployeeView;
import app.view.manager_view.ManagerView;
import app.view.util.ButtonBeautification;
import javafx.geometry.Insets;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;

import java.io.FileNotFoundException;
import java.util.List;

public class LoginView {

    private GridPane loginLayout;

    private Controller controller;

    private TextField username;

    private PasswordField password;

    public LoginView(Controller controller) {
        this.controller = controller;
    }

    /**
     * display user log in
     *
     * @return - a gridPane
     */
    public Parent display() {
        loginLayout = new GridPane();
        loginLayout.setHgap(8);
        loginLayout.setVgap(8);

        Label logInLabel = new Label("Log In");
        logInLabel.setFont(Font.font("Arial", FontWeight.BOLD, 20));
        loginLayout.add(logInLabel, 3, 3);

        Label usernameLabel = new Label("Username");
        username = new TextField();
        VBox usernameBox = new VBox(usernameLabel, username);
        loginLayout.add(usernameBox, 3, 5);

        Label passwordLabel = new Label("Password");
        password = new PasswordField();
        VBox passwordBox = new VBox(passwordLabel, password);
        loginLayout.add(passwordBox, 3, 6);

        Button loginButton = buildLoginButton();
        loginLayout.add(loginButton, 3, 9);

        return loginLayout;
    }

    private Button buildLoginButton() {
        Button loginButton = new Button("Log in");
        loginButton.setOnAction(e -> {
            try {
                User user = controller.getUserByUsernameAndPassword(username.getText(), password.getText());
                if (user.getUserRole().equals(UserRoles.Admin.name())) {
                    Main.window.setScene(buildMainDisplayByUserRole(user, null));
                } else {
                    Stage shopsForUserLayout = buildShopsForUserLayout(user);
                    shopsForUserLayout.show();
                }
            } catch (Exception exception) {
                Label errorText = new Label("We can't reach your account!");
                Label errorText1 = new Label("Username or password incorrect!");
                VBox textLayout = new VBox(errorText, errorText1);
                loginLayout.add(textLayout, 3, 11);
            }
        });

        loginButton.setTextFill(Color.WHITE);
        loginButton.setBackground(new Background(new BackgroundFill(Color.PURPLE, new CornerRadii(5), Insets.EMPTY)));

        return loginButton;
    }

    /**
     * display the stores where the user works/manages them
     *
     * @param user - app user
     */
    private Stage buildShopsForUserLayout(User user) {
        Stage shopsForUserLayout = new Stage();

        TableView<Shop> shopsAvailable = getShopTableView(user);

        Button goToWorkButton = new Button("Go to work");
        goToWorkButton.setOnAction(e -> {
            try {
                Main.window.setScene(buildMainDisplayByUserRole(user, shopsAvailable.getSelectionModel().getSelectedItem()));
                shopsForUserLayout.close();
            } catch (FileNotFoundException fileNotFoundException) {
                fileNotFoundException.printStackTrace();
            }
        });
        ButtonBeautification.beautifyTheButton(goToWorkButton);

        Label infoLabel = new Label("Available shops for you");
        infoLabel.setFont(Font.font("Arial", FontWeight.BOLD, 18));
        StackPane infoPane = new StackPane(infoLabel);

        Label infoLabel1 = new Label("Select one and then press Go to work button!");
        infoLabel1.setFont(Font.font("Arial", FontWeight.BOLD, 11));

        GridPane gridPane = new GridPane();
        gridPane.add(infoPane, 0, 0);
        gridPane.add(infoLabel1, 0, 1);
        gridPane.add(shopsAvailable, 0, 2);
        gridPane.add(goToWorkButton, 0, 3);
        gridPane.setVgap(10);
        gridPane.setPadding(new Insets(20, 20, 20, 20));
        gridPane.setBackground(new Background(new BackgroundFill(Color.DARKGRAY, CornerRadii.EMPTY, Insets.EMPTY)));

        shopsForUserLayout.setScene(new Scene(gridPane, 330, 460));
        return shopsForUserLayout;
    }


    private TableView<Shop> getShopTableView(User user) {
        TableView<Shop> shopsAvailable = new TableView<>();

        TableColumn<Shop, String> shopNameColumn = new TableColumn<>("Shop name");
        shopNameColumn.setCellValueFactory(new PropertyValueFactory<>("name"));
        shopNameColumn.setMinWidth(110);

        TableColumn<Shop, String> addressColumn = new TableColumn<>("Address");
        addressColumn.setCellValueFactory(new PropertyValueFactory<>("address"));
        addressColumn.setMinWidth(140);

        shopsAvailable.getColumns().addAll(shopNameColumn, addressColumn);

        List<Shop> shopsForUser = controller.findShopsAvailableForUser(user);
        shopsAvailable.getItems().addAll(shopsForUser);
        return shopsAvailable;
    }

    private Scene buildMainDisplayByUserRole(User user, Shop shop) throws IllegalArgumentException, FileNotFoundException {
        switch (UserRoles.valueOf(user.getUserRole())) {
            case Admin:
                return new Scene(new AdminView(controller).display(), 700, 500);

            case Manager:
                return new Scene(new ManagerView(controller).display(user, shop), 700, 500);

            case Employee:
                return new Scene(new EmployeeView(controller).display(user, shop), 700, 500);
        }
        return null;
    }

}
