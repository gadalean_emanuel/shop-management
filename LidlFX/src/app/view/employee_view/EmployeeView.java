package app.view.employee_view;

import app.Main;
import app.repository.constants.DateFormat;
import app.controller.Controller;
import app.model.Encashment;
import app.model.Product;
import app.model.Shop;
import app.model.User;
import app.repository.util.Convertor;
import app.view.LoginView;
import app.view.util.ButtonBeautification;
import app.view.util.SponsorIcons;
import javafx.geometry.Insets;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.Node;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.time.LocalDate;

public class EmployeeView {

    Controller controller;

    BorderPane employeeLayout;

    TableView<Encashment> encashmentTableView;

    TableView<Product> products = new TableView<>();

    public EmployeeView(Controller controller) {
        this.controller = controller;
    }

    public Parent display(User user, Shop shop) throws FileNotFoundException {
        Main.window.setTitle("Lidl-Employee");

        employeeLayout = new BorderPane();

        employeeLayout.setTop(buildTopLayout());
        employeeLayout.setCenter(buildCenter(user, shop));
        employeeLayout.setRight(SponsorIcons.build());

        employeeLayout.setBackground(new Background(new BackgroundFill(Color.DARKGRAY, CornerRadii.EMPTY, Insets.EMPTY)));
        return employeeLayout;
    }

    private Node buildTopLayout() {
        MenuItem logOut = new MenuItem("Log out");
        logOut.setOnAction(e -> Main.window.setScene(new Scene(new LoginView(controller).display(), 260, 300)));

        Menu fileMenu = new Menu("File");
        MenuItem exit = new MenuItem("Exit");
        exit.setOnAction(e -> Main.window.close());
        fileMenu.getItems().addAll(logOut, exit);

        MenuBar menuBar = new MenuBar();
        menuBar.getMenus().addAll(fileMenu);
        return menuBar;
    }

    private Stage buildShopDepositLayout(Shop shop) throws FileNotFoundException {
        Stage stage = new Stage();
        stage.setTitle("Deposit");
        stage.getIcons().add(new Image(new FileInputStream("C:\\Users\\Emanuel Gadalean\\Links\\depo.png")));

        products = new TableView<>();

        TableColumn<Product, String> productName = new TableColumn<>("Product");
        productName.setCellValueFactory(new PropertyValueFactory<>("name"));

        TableColumn<Product, Integer> productQuantity = new TableColumn<>("Quantity");
        productQuantity.setCellValueFactory(new PropertyValueFactory<>("quantityInContainers"));
        productQuantity.setMinWidth(100);

        products.getColumns().addAll(productName, productQuantity);
        products.getItems().addAll(controller.findProductsByShopId(shop.getId()));

        StackPane stackPane = new StackPane(products);
        stackPane.setPadding(new Insets(20, 20, 20, 20));

        stage.setScene(new Scene(stackPane, 260, 400));
        return stage;
    }

    private Node buildCenter(User user, Shop shop) {
        Label userRole = new Label("User Role:");
        userRole.setFont(Font.font("Arial", FontWeight.BOLD, 12));
        Label userRoleLabel = new Label(user.getUserRole());
        HBox userRoleBox = new HBox(userRole, userRoleLabel);
        userRoleBox.setSpacing(5);

        Label userStatusLabel = new Label("Status:");
        userStatusLabel.setFont(Font.font("Arial", FontWeight.BOLD, 12));
        Label userStatusL = new Label(user.getUserStatus());
        HBox userStatusBox = new HBox(userStatusLabel, userStatusL);
        userStatusBox.setSpacing(5);

        Label usernameLabel = new Label("Username:");
        usernameLabel.setFont(Font.font("Arial", FontWeight.BOLD, 12));
        Label usernameInfo = new Label(user.getUsername());
        HBox usernameBox = new HBox(usernameLabel, usernameInfo);
        usernameBox.setSpacing(5);

        Label goTo = new Label("Go to:");
        goTo.setFont(Font.font("Impact", FontWeight.BOLD, 20));

        Button productsInventory = buildDepositButton(shop);

        Button encashments = buildEncashmentsButton(shop);

        Button collectsButton = buildCollectsButton(shop);


        VBox buttonsBox = new VBox(goTo, productsInventory, encashments, collectsButton);
        buttonsBox.setSpacing(5);

        VBox labelsVBox = new VBox(usernameBox, userStatusBox, userRoleBox);
        labelsVBox.setSpacing(10);
        labelsVBox.setBackground(new Background(new BackgroundFill(Color.LIGHTBLUE, new CornerRadii(5), Insets.EMPTY)));
        labelsVBox.setPadding(new Insets(5, 5, 5, 5));


        VBox boxForAll = new VBox(labelsVBox, buttonsBox);
        boxForAll.setSpacing(40);
        boxForAll.setPadding(new Insets(10, 10, 10, 10));
        BorderPane.setMargin(boxForAll, new Insets(0, 350, 0, 0));

        return boxForAll;
    }

    private Button buildCollectsButton(Shop shop) {
        Button collectsButton = new Button("COLLECT NOW");
        collectsButton.setOnAction(e -> {
            try {
                Stage cashRegisterView = buildCashRegister(shop);
                cashRegisterView.show();
            } catch (FileNotFoundException fileNotFoundException) {
                fileNotFoundException.printStackTrace();
            }
        });
        collectsButton.setBorder(new Border(new BorderStroke(Color.BLACK, BorderStrokeStyle.SOLID, new CornerRadii(5), BorderWidths.DEFAULT)));
        collectsButton.setBackground(new Background(new BackgroundFill(Color.RED, new CornerRadii(10), Insets.EMPTY)));
        collectsButton.setTextFill(Color.WHITE);
        collectsButton.setMinWidth(130);
        return collectsButton;
    }

    private Button buildEncashmentsButton(Shop shop) {
        Button encashments = new Button("Encashments");
        encashments.setOnAction(e -> {
            try {
                Stage encashmentsTableView = buildEncashmentsTableView(shop);
                encashmentsTableView.show();
            } catch (FileNotFoundException fileNotFoundException) {
                fileNotFoundException.printStackTrace();
            }
        });
        ButtonBeautification.beautifyTheButton(encashments);
        encashments.setMinWidth(130);
        return encashments;
    }

    private Button buildDepositButton(Shop shop) {
        Button productsInventory = new Button("Deposit");
        productsInventory.setOnAction(e -> {
            try {
                Stage shopDepositLayoutView = buildShopDepositLayout(shop);
                shopDepositLayoutView.show();
            } catch (FileNotFoundException fileNotFoundException) {
                fileNotFoundException.printStackTrace();
            }
        });
        ButtonBeautification.beautifyTheButton(productsInventory);
        productsInventory.setMinWidth(130);
        return productsInventory;
    }

    private Stage buildEncashmentsTableView(Shop shop) throws FileNotFoundException {
        Stage stage = new Stage();
        stage.setTitle("Encashments");
        stage.getIcons().add(new Image(new FileInputStream("C:\\Users\\Emanuel Gadalean\\Links\\bani.png")));

        Label dateLabel = new Label("Date:");
        dateLabel.setFont(new Font(15));

        TextField dateField = new TextField();
        dateField.setText(LocalDate.now().format(DateFormat.DATE_FORMATTER));
        dateField.setEditable(false);
        HBox boxDate = new HBox(dateLabel, dateField);
        boxDate.setSpacing(10);

        encashmentTableView = new TableView<>();

        TableColumn<Encashment, String> productNameColumn = new TableColumn<>("Product");
        productNameColumn.setCellValueFactory(new PropertyValueFactory<>("productName"));

        TableColumn<Encashment, Integer> numOfProdColumn = new TableColumn<>("Num of prod");
        numOfProdColumn.setCellValueFactory(new PropertyValueFactory<>("numOfProducts"));

        TableColumn<Encashment, Float> valueColumn = new TableColumn<>("Value");
        valueColumn.setCellValueFactory(new PropertyValueFactory<>("value"));

        encashmentTableView.getColumns().addAll(productNameColumn, numOfProdColumn, valueColumn);
        encashmentTableView.getItems().addAll(controller.findAllEncashmentsByShopId(shop.getId()));

        GridPane tablePane = new GridPane();
        tablePane.add(boxDate, 0, 0);
        tablePane.add(encashmentTableView, 0, 1);
        tablePane.setVgap(17);
        tablePane.setPadding(new Insets(20, 20, 20, 20));

        stage.setScene(new Scene(tablePane, 350, 400));
        return stage;
    }

    private Stage buildCashRegister(Shop shop) throws FileNotFoundException {
        Stage stage = new Stage();
        stage.setTitle("Encashment record");
        stage.getIcons().add(new Image(new FileInputStream("C:\\Users\\Emanuel Gadalean\\Links\\cart.jpg")));

        Label nameLabel = new Label("Select product:");
        ComboBox<Product> productsCB = new ComboBox<>();
        productsCB.getItems().addAll(controller.findProductsByShopId(shop.getId()));
        Convertor.convertProductsComboDisplayList(productsCB);


        Label costLabel = new Label("Encashment value(LEI):");
        TextField costField = new TextField();
        costField.setEditable(false);
        productsCB.setOnAction(e -> {
            Product product = productsCB.getValue();
            costField.setText(product.getCost() + "");
        });

        Label numOfProd = new Label("Num of products:");
        TextField numOfProdField = new TextField();
        numOfProdField.setText("1");
        numOfProdField.textProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue != null && !newValue.isEmpty()) {
                int value = Integer.parseInt(newValue);
                costField.setText(productsCB.getValue().getCost() * value + "");
            } else {
                costField.setText(0 + "");
            }
        });

        Label dateLabel = new Label("Date:");
        TextField dateField = new TextField();
        dateField.setEditable(false);
        dateField.setText(LocalDate.now().format(DateFormat.DATE_FORMATTER));

        Button finished = new Button("Finished");
        finished.setOnAction(e -> {
            controller.insertEncashmentToAShop(productsCB.getSelectionModel().getSelectedItem().getName(), costField.getText(), numOfProdField.getText(), dateField.getText(), shop.getId());
            controller.updateDeposit(productsCB.getSelectionModel().getSelectedItem().getName(), Integer.parseInt(numOfProdField.getText()), shop.getId());
            products.getItems().removeAll(controller.findProductsByShopId(shop.getId()));
            products.getItems().addAll(controller.findProductsByShopId(shop.getId()));
            stage.close();
        });
        ButtonBeautification.beautifyTheButton(finished);

        GridPane cashRegisterLayout = new GridPane();
        cashRegisterLayout.add(nameLabel, 0, 0);
        cashRegisterLayout.add(productsCB, 1, 0);
        cashRegisterLayout.add(numOfProd, 0, 1);
        cashRegisterLayout.add(numOfProdField, 1, 1);
        cashRegisterLayout.add(costLabel, 0, 2);
        cashRegisterLayout.add(costField, 1, 2);
        cashRegisterLayout.add(dateLabel, 0, 3);
        cashRegisterLayout.add(dateField, 1, 3);
        cashRegisterLayout.add(finished, 1, 4);
        cashRegisterLayout.setPadding(new Insets(20, 20, 20, 20));
        cashRegisterLayout.setVgap(10);
        cashRegisterLayout.setHgap(5);

        cashRegisterLayout.setBackground(new Background(new BackgroundFill(Color.LIGHTGRAY, new CornerRadii(10), Insets.EMPTY)));

        stage.setScene(new Scene(cashRegisterLayout, 390, 220));
        return stage;
    }

}
