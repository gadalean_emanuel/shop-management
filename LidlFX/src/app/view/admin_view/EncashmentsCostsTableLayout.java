package app.view.admin_view;

import app.Main;
import app.controller.Controller;
import app.model.Encashment;
import app.model.Expense;
import app.model.Shop;
import app.view.util.ButtonBeautification;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.time.LocalDate;
import java.util.List;

public class EncashmentsCostsTableLayout extends AdminView {

    TableView<Encashment> encashmentTableView;

    TableView<Expense> costsTableView;

    public EncashmentsCostsTableLayout(Controller controller) {
        super(controller);
    }


    public Node buildRight(Shop shop) {
        Label startDateLabel = new Label("Pick a start date:");
        DatePicker startDate = new DatePicker();

        Label endDateLabel = new Label("Pick an end date:");
        DatePicker endDate = new DatePicker();

        Button showMeButton = new Button("Refresh");
        showMeButton.setOnAction(e -> {
            List<Expense> filteredExpenses = controller.filterShopExpensesByDateInterval(startDate.getValue(), endDate.getValue(), shop.getId());
            List<Encashment> filteredEncashments = controller.filterEncashmentById(startDate.getValue(), endDate.getValue(), shop.getId());
            encashmentTableView.getItems().removeAll(controller.findAllEncashmentsByShopId(shop.getId()));
            encashmentTableView.getItems().addAll(filteredEncashments);

            costsTableView.getItems().removeAll(controller.findAllExpensesByShopId(shop.getId()));
            costsTableView.getItems().addAll(filteredExpenses);

            startDate.setValue(null);
            endDate.setValue(null);
        });
        ButtonBeautification.beautifyTheButton(showMeButton);

        VBox startDateBox = new VBox(startDateLabel, startDate);
        VBox endDateBox = new VBox(endDateLabel, endDate);

        VBox boxForAll = new VBox(startDateBox, endDateBox, showMeButton);
        boxForAll.setSpacing(15);

        boxForAll.setPadding(new Insets(30, 5, 5, 5));
        boxForAll.setBackground(new Background(new BackgroundFill(Color.DARKGRAY, CornerRadii.EMPTY, Insets.EMPTY)));
        return boxForAll;
    }

    /**
     * @param shop -shop that is managed
     * @return - center component of admin main view(borderPane) - contains 2 tables: one for encashments and one for expenses
     */
    public Node buildCenter(Shop shop) {
        TableView<Encashment> encashmentTableView = buildEncashmentTable(shop);

        TableView<Expense> costTableView = buildExpenseTable(shop);

        Label encashmentLabel = new Label("Encashments");
        encashmentLabel.setFont(Font.font("Arial", FontWeight.BOLD, 15));

        Label coostsLabel = new Label("Costs");
        coostsLabel.setFont(Font.font("Arial", FontWeight.BOLD, 15));

        VBox encashmentBox = new VBox(encashmentLabel, encashmentTableView);
        encashmentBox.setSpacing(10);

        VBox expensesBox = new VBox(coostsLabel, costTableView);
        expensesBox.setSpacing(10);

        HBox boxForTables = new HBox(encashmentBox, expensesBox);
        boxForTables.setSpacing(10);
        boxForTables.setPadding(new Insets(5, 5, 5, 5));

        return boxForTables;
    }

    private TableView<Expense> buildExpenseTable(Shop shop) {
        TableView<Expense> costsTableView = new TableView<>();

        TableColumn<Expense, String> nameColumn = new TableColumn<>("Name");
        nameColumn.setCellValueFactory(new PropertyValueFactory<>("name"));
        nameColumn.setMaxWidth(100);

        TableColumn<Expense, Double> costValue = new TableColumn<>("Value(LEI)");
        costValue.setCellValueFactory(new PropertyValueFactory<>("cost"));

        TableColumn<Expense, LocalDate> dateTable = new TableColumn<>("Date");
        dateTable.setCellValueFactory(new PropertyValueFactory<>("date"));

        costsTableView.getColumns().addAll(nameColumn, costValue, dateTable);
        costsTableView.getItems().addAll(controller.findAllExpensesByShopId(shop.getId()));
        costsTableView.setBorder(new Border(new BorderStroke(Color.BLACK, BorderStrokeStyle.SOLID, CornerRadii.EMPTY, BorderWidths.DEFAULT)));
        return costsTableView;
    }

    private TableView<Encashment> buildEncashmentTable(Shop shop) {
        TableView<Encashment> encashmentTableView = new TableView<>();

        TableColumn<Encashment, String> productNameColumn = new TableColumn<>("Product");
        productNameColumn.setCellValueFactory(new PropertyValueFactory<>("productName"));
        productNameColumn.setMaxWidth(90);

        TableColumn<Encashment, Integer> numOfProducts = new TableColumn<>("Num of prod");
        numOfProducts.setCellValueFactory(new PropertyValueFactory<>("numOfProducts"));
        numOfProducts.setMaxWidth(80);


        TableColumn<Encashment, Double> costValueColumn = new TableColumn<>("Value(LEI)");
        costValueColumn.setCellValueFactory(new PropertyValueFactory<>("value"));

        TableColumn<Encashment, LocalDate> dateTableColumn = new TableColumn<>("Date");
        dateTableColumn.setCellValueFactory(new PropertyValueFactory<>("date"));

        encashmentTableView.getColumns().addAll(productNameColumn, numOfProducts, costValueColumn, dateTableColumn);
        encashmentTableView.getItems().addAll(controller.findAllEncashmentsByShopId(shop.getId()));
        encashmentTableView.setBorder(new Border(new BorderStroke(Color.BLACK, BorderStrokeStyle.SOLID, CornerRadii.EMPTY, BorderWidths.DEFAULT)));
        return encashmentTableView;
    }

    /**
     * @return - top component of the admin main view which contains: home button, back button
     * @throws FileNotFoundException -
     */
    public Node buildTop() throws FileNotFoundException {
        Button backButton = buildBackButton();

        Button homeButton = buildHomeButton();

        HBox buttonsBox = new HBox(backButton, homeButton);
        buttonsBox.setSpacing(5);
        buttonsBox.setPadding(new Insets(5, 0, 5, 5));

        return buttonsBox;
    }

    private Button buildBackButton() throws FileNotFoundException {
        Button backButton = new Button();
        ImageView backIcon = new ImageView(new Image(new FileInputStream("C:\\Users\\Emanuel Gadalean\\Links\\back.png")));
        backIcon.setPreserveRatio(true);
        backIcon.setFitWidth(15);
        backButton.setGraphic(backIcon);

        backButton.setOnAction(e -> {
            ShopsTableLayout shopsLayout = new ShopsTableLayout(controller);
            adminLayout = new BorderPane();
            Main.window.setTitle("Lidl-Shops");
            adminLayout.setCenter(shopsLayout.buildCenter());
            adminLayout.setRight(shopsLayout.buildRight());
            adminLayout.setBottom(shopsLayout.buildBottom());
            try {
                adminLayout.setTop(shopsLayout.buildTop());
            } catch (FileNotFoundException fileNotFoundException) {
                fileNotFoundException.printStackTrace();
            }
            adminLayout.setBackground(new Background(new BackgroundFill(Color.DARKGRAY, CornerRadii.EMPTY, Insets.EMPTY)));
            Main.window.setScene(new Scene(adminLayout, 700, 500));
        });
        return backButton;
    }

    private Button buildHomeButton() throws FileNotFoundException {
        Button homeButton = new Button();
        ImageView homeIcon = new ImageView(new Image(new FileInputStream("C:\\Users\\Emanuel Gadalean\\Links\\home.png")));
        homeIcon.setPreserveRatio(true);
        homeIcon.setFitWidth(15);
        homeButton.setGraphic(homeIcon);

        homeButton.setOnAction(e -> {
            try {
                Main.window.setScene(new Scene(new AdminView(controller).display(), 700, 500));
            } catch (FileNotFoundException fileNotFoundException) {
                fileNotFoundException.printStackTrace();
            }
        });
        return homeButton;
    }
}
