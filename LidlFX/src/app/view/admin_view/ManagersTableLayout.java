package app.view.admin_view;

import app.Main;
import app.controller.Controller;
import app.model.Shop;
import app.model.User;
import app.controller.security.Encryption;
import app.view.util.ButtonBeautification;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.List;

public class ManagersTableLayout extends AdminView {

    private TableView<User> managerTable = new TableView<>();

    private TextField nameField;

    private TextField ageField;

    private TextField CNPField;

    private ComboBox<String> genderChoice;

    private TextField addressField;

    private Stage managerRecord;

    public ManagersTableLayout(Controller controller) {
        super(controller);
    }

    public Node buildManagersTable(Shop shop) {
        TableColumn<User, Integer> idColumn = new TableColumn<>("Id");
        idColumn.setCellValueFactory(new PropertyValueFactory<>("id"));

        TableColumn<User, String> nameColumn = new TableColumn<>("Name");
        nameColumn.setCellValueFactory(new PropertyValueFactory<>("name"));
        nameColumn.setMinWidth(120);

        TableColumn<User, Integer> ageColumn = new TableColumn<>("Age");
        ageColumn.setCellValueFactory(new PropertyValueFactory<>("age"));
        ageColumn.setMinWidth(65);

        TableColumn<User, Long> CNPColumn = new TableColumn<>("CNP");
        CNPColumn.setCellValueFactory(new PropertyValueFactory<>("CNP"));
        CNPColumn.setMinWidth(80);

        TableColumn<User, String> genderColumn = new TableColumn<>("Gender");
        genderColumn.setCellValueFactory(new PropertyValueFactory<>("gender"));
        genderColumn.setMinWidth(90);

        TableColumn<User, String> addressColumn = new TableColumn<>("Address");
        addressColumn.setCellValueFactory(new PropertyValueFactory<>("address"));
        addressColumn.setMinWidth(150);

        managerTable.getColumns().addAll(idColumn, nameColumn, ageColumn, CNPColumn, genderColumn, addressColumn);
        managerTable.getItems().addAll(controller.findAllManagersByShopId(shop.getId()));
        managerTable.setBorder(new Border(new BorderStroke(Color.BLACK, BorderStrokeStyle.SOLID, CornerRadii.EMPTY, BorderWidths.DEFAULT)));
        BorderPane.setMargin(managerTable, new Insets(10));
        managerTable.setPadding(new Insets(10, 10, 10, 10));

        return managerTable;
    }

    /**
     * @param shop - the store that is managed
     * @return - right component of the admin main view(borderPane) - which contains buttons
     */
    public Node buildRight(Shop shop) {
        Button addButton = buildAddButton(shop);

        Button deleteButton = buildDeleteButton(shop);

        Button updateButton = buildUpdateButton(shop);

        VBox buttonsBox = new VBox(addButton, deleteButton, updateButton);
        buttonsBox.setSpacing(20);
        buttonsBox.setPadding(new Insets(10, 10, 0, 10));
        buttonsBox.setBackground(new Background(new BackgroundFill(Color.DARKGRAY, CornerRadii.EMPTY, Insets.EMPTY)));

        return buttonsBox;
    }

    private Button buildAddButton(Shop shop) {
        Button addButton = new Button("Add");
        addButton.setOnAction(e -> {
            Stage managerRecordForAdd = buildManagerRecordForAdd(shop);
            managerRecordForAdd.show();
        });
        ButtonBeautification.beautifyTheButton(addButton);
        addButton.setMinWidth(90);
        return addButton;
    }

    private Button buildDeleteButton(Shop shop) {
        Button deleteButton = new Button("Delete");
        deleteButton.setOnAction(e -> {
            controller.removeManager(managerTable.getSelectionModel().getSelectedItem(), shop);
            managerTable.getItems().remove(managerTable.getSelectionModel().getSelectedItem());
            managerTable.getSelectionModel().clearSelection();
        });
        deleteButton.setMinWidth(90);
        ButtonBeautification.beautifyTheButton(deleteButton);
        return deleteButton;
    }

    private Button buildUpdateButton(Shop shop) {
        Button updateButton = new Button("Update");
        updateButton.setOnAction(e -> {
            Stage managerRecordForUpdate = buildManagerRecordForUpdate(managerTable.getSelectionModel().getSelectedItem(), shop);
            managerRecordForUpdate.show();
        });
        updateButton.setMinWidth(90);
        ButtonBeautification.beautifyTheButton(updateButton);
        return updateButton;
    }

    private Stage buildManagerRecordForAdd(Shop shop) {
        managerRecord = new Stage();
        managerRecord.setTitle("Manager record");

        Label employeeLabel = new Label("Personal Data");
        employeeLabel.setFont(Font.font("Arial", FontWeight.BOLD, 17));
        StackPane title = new StackPane(employeeLabel);

        Label nameLabel = new Label("Name:");
        nameField = new TextField();
        HBox nameBox = new HBox(nameLabel, nameField);
        nameBox.setSpacing(12);

        Label ageLabel = new Label("Age:");
        ageField = new TextField();
        HBox ageBox = new HBox(ageLabel, ageField);
        ageBox.setSpacing(12);

        Label CNPLabel = new Label("CNP:");
        CNPField = new TextField();
        HBox CNPBox = new HBox(CNPLabel, CNPField);
        CNPBox.setSpacing(12);

        Label genderLabel = new Label("Gender:");
        genderChoice = new ComboBox<>();
        genderChoice.getItems().addAll("Masculin", "Feminin", "Altele");
        HBox genderBox = new HBox(genderLabel, genderChoice);
        genderBox.setSpacing(12);

        Label addressLabel = new Label("Address:");
        addressField = new TextField();
        HBox addressBox = new HBox(addressLabel, addressField);
        addressBox.setSpacing(12);

        Label employeeAccountLabel = new Label("Account Data");
        employeeAccountLabel.setFont(Font.font("Arial", FontWeight.BOLD, 17));
        StackPane employeeAccountTitle = new StackPane(employeeAccountLabel);

        Label usernameLabel = new Label("Username:");
        TextField usernameField = new TextField();
        HBox usernameBox = new HBox(usernameLabel, usernameField);
        usernameBox.setSpacing(10);

        Label passwordLabel = new Label("Password:");
        TextField passwordField = new TextField();
        HBox passwordBox = new HBox(passwordLabel, passwordField);
        passwordBox.setSpacing(10);

        GridPane managerGrid = new GridPane();

        Button addButton = new Button("Finished");
        addButton.setOnAction(e -> {
            if (nameField.getText() != null && ageField.getText() != null && CNPField.getText() != null && addressField.getText() != null && genderChoice.getValue() != null) {
                controller.insertManager(nameField.getText(), Integer.parseInt(ageField.getText()), Long.parseLong(CNPField.getText()), genderChoice.getValue(), addressField.getText(), usernameField.getText(), passwordField.getText(), shop);

                List<User> users = controller.findAllUsers();
                managerTable.getItems().add(users.get(users.size() - 1));
                managerRecord.close();
            } else {
                Label errorLabel = new Label("Please complete all fields!");
                errorLabel.setTextFill(Color.RED);
                managerGrid.add(errorLabel, 0, 9);
            }
        });
        ButtonBeautification.beautifyTheButton(addButton);
        HBox buttonBox = new HBox(addButton);
        buttonBox.setPadding(new Insets(0, 0, 0, 150));

        managerGrid.add(title, 0, 1);
        managerGrid.add(employeeAccountTitle, 7, 1);
        managerGrid.add(nameBox, 0, 2);
        managerGrid.add(ageBox, 0, 3);
        managerGrid.add(CNPBox, 0, 4);
        managerGrid.add(genderBox, 0, 5);
        managerGrid.add(addressBox, 0, 6);
        managerGrid.add(usernameBox, 7, 2);
        managerGrid.add(passwordBox, 7, 3);
        managerGrid.add(buttonBox, 7, 4);
        managerGrid.setVgap(18);
        managerGrid.setHgap(10);
        managerGrid.setPadding(new Insets(20, 20, 20, 20));
        managerGrid.setBackground(new Background(new BackgroundFill(Color.LIGHTGRAY, new CornerRadii(10), Insets.EMPTY)));

        managerRecord.setScene(new Scene(managerGrid, 600, 350));
        return managerRecord;
    }

    private Stage buildManagerRecordForUpdate(User manager, Shop shop) {
        managerRecord = new Stage();
        managerRecord.setTitle("Manager record");

        Label employeeLabel = new Label("Personal Data");
        employeeLabel.setFont(Font.font("Arial", FontWeight.BOLD, 17));
        StackPane title = new StackPane(employeeLabel);

        Label nameLabel = new Label("Name");
        nameField = new TextField();
        nameField.setText(manager.getName());
        HBox nameBox = new HBox(nameLabel, nameField);
        nameBox.setSpacing(5);

        Label ageLabel = new Label("Age");
        ageField = new TextField();
        ageField.setText(manager.getAge() + "");
        HBox ageBox = new HBox(ageLabel, ageField);
        ageBox.setSpacing(5);

        Label CNPLabel = new Label("CNP");
        CNPField = new TextField();
        CNPField.setText(manager.getCNP() + "");
        HBox CNPBox = new HBox(CNPLabel, CNPField);
        CNPBox.setSpacing(5);

        Label genderLabel = new Label("Gender");
        genderChoice = new ComboBox<>();
        genderChoice.getItems().addAll("Masculin", "Feminin", "Altele");
        genderChoice.setValue(manager.getGender());
        HBox genderBox = new HBox(genderLabel, genderChoice);
        genderBox.setSpacing(5);

        Label addressLabel = new Label("Address");
        addressField = new TextField();
        addressField.setText(manager.getAddress());
        HBox addressBox = new HBox(addressLabel, addressField);
        addressBox.setSpacing(5);

        Label employeeAccountLabel = new Label("Account Data");
        employeeAccountLabel.setFont(Font.font("Arial", FontWeight.BOLD, 17));
        StackPane employeeAccountTitle = new StackPane(employeeAccountLabel);

        Label usernameLabel = new Label("Username:");
        TextField usernameField = new TextField();
        usernameField.setText(manager.getUsername());
        HBox usernameBox = new HBox(usernameLabel, usernameField);
        usernameBox.setSpacing(10);

        Label passwordLabel = new Label("Password:");
        TextField passwordField = new TextField();
        passwordField.setText(Encryption.decryption(manager.getPassword()));
        HBox passwordBox = new HBox(passwordLabel, passwordField);
        passwordBox.setSpacing(10);

        GridPane managerGrid = new GridPane();
        Button addButton = new Button("Finished");
        addButton.setOnAction(e -> {
            if (nameField.getText() != null && ageField.getText() != null && CNPField.getText() != null && addressField.getText() != null && genderChoice.getValue() != null) {
                controller.updateManager(manager, nameField.getText(), Integer.parseInt(ageField.getText()), Long.parseLong(CNPField.getText()), genderChoice.getValue(), addressField.getText(), usernameField.getText(), passwordField.getText(), shop);
                managerTable.getItems().removeAll(controller.findAllManagersByShopId(shop.getId()));
                managerTable.getItems().addAll(controller.findAllManagersByShopId(shop.getId()));
                managerRecord.close();
                managerTable.getSelectionModel().clearSelection();
            } else {
                Label errorLabel = new Label("Please complete all fields!");
                errorLabel.setTextFill(Color.RED);
                managerGrid.add(errorLabel, 0, 9);
            }
        });
        ButtonBeautification.beautifyTheButton(addButton);

        HBox buttonBox = new HBox(addButton);
        buttonBox.setPadding(new Insets(0, 0, 0, 150));

        managerGrid.add(title, 0, 1);
        managerGrid.add(employeeAccountTitle, 7, 1);
        managerGrid.add(nameBox, 0, 2);
        managerGrid.add(ageBox, 0, 3);
        managerGrid.add(CNPBox, 0, 4);
        managerGrid.add(genderBox, 0, 5);
        managerGrid.add(addressBox, 0, 6);
        managerGrid.add(usernameBox, 7, 2);
        managerGrid.add(passwordBox, 7, 3);
        managerGrid.add(buttonBox, 7, 4);
        managerGrid.setVgap(18);
        managerGrid.setHgap(10);
        managerGrid.setPadding(new Insets(20, 20, 20, 20));
        managerGrid.setBackground(new Background(new BackgroundFill(Color.LIGHTGRAY, new CornerRadii(10), Insets.EMPTY)));

        managerRecord.setScene(new Scene(managerGrid, 600, 350));
        return managerRecord;
    }

    /**
     * @return - top component of the admin main view which contains: home button, back button
     * @throws FileNotFoundException -
     */
    public Node buildTopLayout() throws FileNotFoundException {
        Button backButton = buildBackButton();

        Button homeButton = buildHomeButton();

        HBox buttonsBox = new HBox(backButton, homeButton);
        buttonsBox.setSpacing(5);
        buttonsBox.setPadding(new Insets(5, 0, 5, 5));

        return buttonsBox;
    }

    private Button buildHomeButton() throws FileNotFoundException {
        Button homeButton = new Button();
        ImageView homeIcon = new ImageView(new Image(new FileInputStream("C:\\Users\\Emanuel Gadalean\\Links\\home.png")));
        homeIcon.setPreserveRatio(true);
        homeIcon.setFitWidth(15);
        homeButton.setGraphic(homeIcon);

        homeButton.setOnAction(e -> {
            try {
                Main.window.setScene(new Scene(new AdminView(controller).display(), 700, 500));
            } catch (FileNotFoundException fileNotFoundException) {
                fileNotFoundException.printStackTrace();
            }
        });
        return homeButton;
    }

    private Button buildBackButton() throws FileNotFoundException {
        Button backButton = new Button();
        ImageView backIcon = new ImageView(new Image(new FileInputStream("C:\\Users\\Emanuel Gadalean\\Links\\back.png")));
        backIcon.setPreserveRatio(true);
        backIcon.setFitWidth(15);
        backButton.setGraphic(backIcon);

        backButton.setOnAction(e -> {
            ShopsTableLayout shopsLayout = new ShopsTableLayout(controller);
            adminLayout = new BorderPane();
            Main.window.setTitle("Lidl-Shops");
            adminLayout.setCenter(shopsLayout.buildCenter());
            adminLayout.setRight(shopsLayout.buildRight());
            adminLayout.setBottom(shopsLayout.buildBottom());
            try {
                adminLayout.setTop(shopsLayout.buildTop());
            } catch (FileNotFoundException fileNotFoundException) {
                fileNotFoundException.printStackTrace();
            }
            adminLayout.setBackground(new Background(new BackgroundFill(Color.DARKGRAY, CornerRadii.EMPTY, Insets.EMPTY)));
            Main.window.setScene(new Scene(adminLayout, 700, 500));
        });
        return backButton;
    }

}
