package app.view.admin_view;

import app.Main;
import app.controller.Controller;
import app.view.LoginView;
import app.view.util.ButtonBeautification;
import app.view.util.SponsorIcons;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

public class AdminView {

    public BorderPane adminLayout;

    Controller controller;

    public AdminView(Controller controller) {
        this.controller = controller;
    }

    public Parent display() throws FileNotFoundException {
        Main.window.setTitle("Lidl-Admin");

        adminLayout = new BorderPane();

        adminLayout.setTop(buildTopLayout());
        try {
            adminLayout.setCenter(buildCenter());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        try {
            adminLayout.setRight(SponsorIcons.build());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        adminLayout.setBackground(new Background(new BackgroundFill(Color.DARKGRAY, CornerRadii.EMPTY, Insets.EMPTY)));

        return adminLayout;
    }

    /**
     * @return - top component of the admin main view(borderPane) which is a menu bar
     * @throws FileNotFoundException -
     */
    public Node buildTopLayout() throws FileNotFoundException {
        MenuItem logOut = new MenuItem("Log out");
        logOut.setOnAction(e -> Main.window.setScene(new Scene(new LoginView(controller).display(), 260, 300)));

        Menu fileMenu = new Menu("File");
        MenuItem exit = new MenuItem("Exit");
        exit.setOnAction(e -> Main.window.close());
        fileMenu.getItems().addAll(logOut, exit);

        MenuBar menuBar = new MenuBar();
        menuBar.getMenus().addAll(fileMenu);

        return menuBar;
    }

    /**
     * @return - center component of the admin main view(borderPane)
     * @throws FileNotFoundException -
     */
    private Node buildCenter() throws FileNotFoundException {
        ImageView imageView = getLidlImage();

        Button shops = buildShopsButton();

        Label goTo = new Label("Go to:");
        goTo.setFont(Font.font("Impact", FontWeight.BOLD, 20));

        VBox vBox = new VBox(goTo, shops);
        vBox.setSpacing(10);

        GridPane gridPane = new GridPane();
        gridPane.add(imageView, 0, 0);
        gridPane.add(vBox, 0, 1);
        gridPane.setVgap(5);
        gridPane.setPadding(new Insets(15, 15, 15, 15));
        return gridPane;
    }

    private Button buildShopsButton() {
        Button shops = new Button("Shops");
        shops.setOnAction(e -> {
            ShopsTableLayout shopsLayout = new ShopsTableLayout(controller);

            Main.window.setTitle("Lidl-Shops");
            adminLayout.setCenter(shopsLayout.buildCenter());
            adminLayout.setRight(shopsLayout.buildRight());
            adminLayout.setBottom(shopsLayout.buildBottom());
            try {
                adminLayout.setTop(shopsLayout.buildTop());
            } catch (FileNotFoundException fileNotFoundException) {
                fileNotFoundException.printStackTrace();
            }
        });
        ButtonBeautification.beautifyTheButton(shops);
        shops.setMinWidth(130);
        return shops;
    }

    private ImageView getLidlImage() throws FileNotFoundException {
        Image lidlImage = new Image(new FileInputStream("C:\\Users\\Emanuel Gadalean\\Links\\poza.jpg"));
        ImageView imageView = new ImageView(lidlImage);
        imageView.setFitHeight(350);
        imageView.setFitWidth(400);
        imageView.setPreserveRatio(true);
        imageView.setX(50);
        imageView.setY(50);
        return imageView;
    }

}
