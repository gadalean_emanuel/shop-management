package app.view.admin_view;

import app.Main;
import app.controller.Controller;
import app.model.Shop;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

public class ShopsTableLayout extends AdminView {

    private TableView<Shop> shopsTable;

    public ShopsTableLayout(Controller controller) {
        super(controller);
    }

    public Node buildCenter() {
        TableColumn<Shop, Integer> idColumn = new TableColumn<>("Id");
        idColumn.setCellValueFactory(new PropertyValueFactory<>("id"));
        idColumn.setMinWidth(70);

        TableColumn<Shop, String> nameColumn = new TableColumn<>("Name");
        nameColumn.setCellValueFactory(new PropertyValueFactory<>("name"));
        nameColumn.setMinWidth(170);

        TableColumn<Shop, String> addressColumn = new TableColumn<>("Address");
        addressColumn.setCellValueFactory(new PropertyValueFactory<>("address"));
        addressColumn.setMinWidth(210);

        shopsTable = new TableView<>();
        shopsTable.getColumns().addAll(idColumn, nameColumn, addressColumn);
        shopsTable.getItems().addAll(controller.findAllShops());
        shopsTable.setBorder(new Border(new BorderStroke(Color.BLACK, BorderStrokeStyle.SOLID, CornerRadii.EMPTY, BorderWidths.DEFAULT)));
        BorderPane.setMargin(shopsTable, new Insets(5));
        shopsTable.setPadding(new Insets(10, 10, 10, 10));

        return shopsTable;
    }

    public Node buildRight() {
        Label selectInfoLabel = new Label("Select shop and go to");
        selectInfoLabel.setFont(Font.font("Arial", FontWeight.BOLD, 12));
        StackPane labelPane = new StackPane(selectInfoLabel);

        Button managersButton = new Button("Managers");
        managersButton.setOnAction(e -> {
            ManagersTableLayout managersLayout = new ManagersTableLayout(controller);
            adminLayout = new BorderPane();
            Main.window.setTitle("Lidl-Managers");
            adminLayout.setCenter(managersLayout.buildManagersTable(shopsTable.getSelectionModel().getSelectedItem()));
            adminLayout.setRight(managersLayout.buildRight(shopsTable.getSelectionModel().getSelectedItem()));
            try {
                adminLayout.setTop(managersLayout.buildTopLayout());
            } catch (FileNotFoundException fileNotFoundException) {
                fileNotFoundException.printStackTrace();
            }

            adminLayout.setBackground(new Background(new BackgroundFill(Color.DARKGRAY, CornerRadii.EMPTY, Insets.EMPTY)));
            Main.window.setScene(new Scene(adminLayout, 700, 500));
        });
        managersButton.setBackground(new Background(new BackgroundFill(Color.MIDNIGHTBLUE, new CornerRadii(10), Insets.EMPTY)));
        managersButton.setTextFill(Color.WHITE);
        managersButton.setBorder(new Border(new BorderStroke(Color.BLACK, BorderStrokeStyle.SOLID, new CornerRadii(5), BorderWidths.DEFAULT)));
        managersButton.setMinWidth(150);


        Button encashmentsButton = new Button("Encashments & Costs");
        encashmentsButton.setOnAction(e -> {
            EncashmentsCostsTableLayout encashmentsCostsTableLayout = new EncashmentsCostsTableLayout(controller);
            adminLayout = new BorderPane();
            Main.window.setTitle("Lidl-Managers");
            adminLayout.setCenter(encashmentsCostsTableLayout.buildCenter(shopsTable.getSelectionModel().getSelectedItem()));
            adminLayout.setRight(encashmentsCostsTableLayout.buildRight(shopsTable.getSelectionModel().getSelectedItem()));
            try {
                adminLayout.setTop(encashmentsCostsTableLayout.buildTop());
            } catch (FileNotFoundException fileNotFoundException) {
                fileNotFoundException.printStackTrace();
            }

            adminLayout.setBackground(new Background(new BackgroundFill(Color.DARKGRAY, CornerRadii.EMPTY, Insets.EMPTY)));
            Main.window.setScene(new Scene(adminLayout, 800, 500));

        });
        encashmentsButton.setBackground(new Background(new BackgroundFill(Color.MIDNIGHTBLUE, new CornerRadii(10), Insets.EMPTY)));
        encashmentsButton.setTextFill(Color.WHITE);
        encashmentsButton.setBorder(new Border(new BorderStroke(Color.BLACK, BorderStrokeStyle.SOLID, new CornerRadii(5), BorderWidths.DEFAULT)));
        encashmentsButton.setMinWidth(150);

        Button clearButton = new Button("Clear");
        clearButton.setOnAction(e -> shopsTable.getSelectionModel().clearSelection());
        clearButton.setMinWidth(80);
        clearButton.setBackground(new Background(new BackgroundFill(Color.PALEVIOLETRED, new CornerRadii(10), Insets.EMPTY)));
        clearButton.setTextFill(Color.WHITE);

        VBox textFields = new VBox(managersButton, encashmentsButton, clearButton);
        textFields.setSpacing(10);
        textFields.setPadding(new Insets(10, 5, 0, 3));
        textFields.setBackground(new Background(new BackgroundFill(Color.DARKGRAY, CornerRadii.EMPTY, Insets.EMPTY)));

        GridPane gridPaneForAll = new GridPane();
        gridPaneForAll.add(labelPane, 0, 0);
        gridPaneForAll.add(textFields, 0, 1);
        gridPaneForAll.setVgap(20);
        gridPaneForAll.setPadding(new Insets(10, 5, 0, 3));
        gridPaneForAll.setBackground(new Background(new BackgroundFill(Color.DARKGRAY, CornerRadii.EMPTY, Insets.EMPTY)));
        return gridPaneForAll;
    }

    public Node buildBottom() {
        Button addButton = new Button("Add shop");
        addButton.setOnAction(e -> buildShopRecordForAdd());
        addButton.setBackground(new Background(new BackgroundFill(Color.MIDNIGHTBLUE, new CornerRadii(10), Insets.EMPTY)));
        addButton.setTextFill(Color.WHITE);

        Button updateButton = new Button("Update shop");
        updateButton.setOnAction(e -> buildShopRecordForUpdate(shopsTable.getSelectionModel().getSelectedItem()));
        updateButton.setBackground(new Background(new BackgroundFill(Color.MIDNIGHTBLUE, new CornerRadii(10), Insets.EMPTY)));
        updateButton.setTextFill(Color.WHITE);

        HBox buttonsBox = new HBox(addButton, updateButton);
        buttonsBox.setSpacing(15);
        buttonsBox.setPadding(new Insets(10, 20, 20, 5));
        return buttonsBox;
    }

    private void buildShopRecordForUpdate(Shop selectedShop) {
        Stage shopRecord = new Stage();
        shopRecord.setTitle("Shop record");

        Label shopNameLabel = new Label("Shop name");
        TextField shopNameField = new TextField();
        shopNameField.setText(selectedShop.getName());
        HBox shopNameBox = new HBox(shopNameLabel, shopNameField);
        shopNameBox.setSpacing(10);

        Label addressLabel = new Label("Shop address");
        TextField addressField = new TextField();
        addressField.setText(selectedShop.getAddress());
        HBox addressBox = new HBox(addressLabel, addressField);
        addressBox.setSpacing(10);

        Button addButton = new Button("Finished");
        addButton.setOnAction(e -> {
            controller.updateShop(selectedShop, shopNameField.getText(), addressField.getText());
            shopsTable.getItems().removeAll(controller.findAllShops());
            shopsTable.getItems().addAll(controller.findAllShops());
            shopRecord.close();
        });
        addButton.setBackground(new Background(new BackgroundFill(Color.MIDNIGHTBLUE, new CornerRadii(10), Insets.EMPTY)));
        addButton.setTextFill(Color.WHITE);

        GridPane shopRecordPane = new GridPane();
        shopRecordPane.add(shopNameBox, 0, 0);
        shopRecordPane.add(addressBox, 0, 1);
        shopRecordPane.add(addButton, 0, 2);
        shopRecordPane.setVgap(20);
        shopRecordPane.setPadding(new Insets(20, 20, 20, 20));

        shopRecord.setScene(new Scene(shopRecordPane, 300, 200));
        shopRecord.show();
    }

    private void buildShopRecordForAdd() {
        Stage shopRecord = new Stage();
        shopRecord.setTitle("Shop record");

        Label shopNameLabel = new Label("Shop name");
        TextField shopNameField = new TextField();
        HBox shopNameBox = new HBox(shopNameLabel, shopNameField);
        shopNameBox.setSpacing(10);

        Label addressLabel = new Label("Shop address");
        TextField addressField = new TextField();
        HBox addressBox = new HBox(addressLabel, addressField);
        addressBox.setSpacing(10);

        Button addButton = new Button("Finished");
        addButton.setOnAction(e -> {
            controller.insertShop(shopNameField.getText(), addressField.getText());
            shopRecord.close();
            shopsTable.getItems().add(controller.findAllShops().get(controller.findAllShops().size() - 1));
        });
        addButton.setBackground(new Background(new BackgroundFill(Color.MIDNIGHTBLUE, new CornerRadii(10), Insets.EMPTY)));
        addButton.setTextFill(Color.WHITE);

        GridPane shopRecordPane = new GridPane();
        shopRecordPane.add(shopNameBox, 0, 0);
        shopRecordPane.add(addressBox, 0, 1);
        shopRecordPane.add(addButton, 0, 2);
        shopRecordPane.setVgap(20);
        shopRecordPane.setPadding(new Insets(20, 20, 20, 20));

        shopRecord.setScene(new Scene(shopRecordPane, 300, 200));
        shopRecord.show();
    }

    public Node buildTop() throws FileNotFoundException {
        Button homeButton = new Button();
        ImageView homeIcon = new ImageView(new Image(new FileInputStream("C:\\Users\\Emanuel Gadalean\\Links\\home.png")));
        homeIcon.setPreserveRatio(true);
        homeIcon.setFitWidth(15);
        homeButton.setGraphic(homeIcon);
        homeButton.setOnAction(e -> {
            try {
                Main.window.setScene(new Scene(new AdminView(controller).display(), 700, 500));
            } catch (FileNotFoundException fileNotFoundException) {
                fileNotFoundException.printStackTrace();
            }
        });

        Button backButton = new Button();
        ImageView backIcon = new ImageView(new Image(new FileInputStream("C:\\Users\\Emanuel Gadalean\\Links\\back.png")));
        backIcon.setPreserveRatio(true);
        backIcon.setFitWidth(15);
        backButton.setGraphic(backIcon);
        backButton.setOnAction(e -> {
            try {
                Main.window.setScene(new Scene(new AdminView(controller).display(), 700, 500));
            } catch (FileNotFoundException fileNotFoundException) {
                fileNotFoundException.printStackTrace();
            }
        });


        HBox buttonsBox = new HBox(backButton, homeButton);
        buttonsBox.setSpacing(5);
        buttonsBox.setPadding(new Insets(5, 0, 5, 5));

        return buttonsBox;
    }
}
