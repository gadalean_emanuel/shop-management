package app.view.util;

import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;

public class ButtonBeautification {

    public static void beautifyTheButton(Button button) {
        button.setBorder(new Border(new BorderStroke(Color.BLACK, BorderStrokeStyle.SOLID, new CornerRadii(5), BorderWidths.DEFAULT)));
        button.setBackground(new Background(new BackgroundFill(Color.MIDNIGHTBLUE, new CornerRadii(10), Insets.EMPTY)));
        button.setTextFill(Color.WHITE);
    }
}
