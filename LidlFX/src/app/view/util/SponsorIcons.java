package app.view.util;

import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;
import javafx.scene.Node;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

public class SponsorIcons {

    /**
     *
     * @return - a component for a borderPane which contains logos of products companies/ lidl sponsors
     * @throws FileNotFoundException -
     */
    public static Node build() throws FileNotFoundException {
        Label icon1 = new Label();
        ImageView iconView1 = new ImageView(new Image(new FileInputStream("C:\\Users\\Emanuel Gadalean\\Links\\poza1.jpg")));
        iconView1.setPreserveRatio(true);
        iconView1.setFitWidth(100);
        iconView1.setFitHeight(100);
        icon1.setGraphic(iconView1);

        Label icon2 = new Label();
        ImageView iconView2 = new ImageView(new Image(new FileInputStream("C:\\Users\\Emanuel Gadalean\\Links\\poza2.jpg")));
        iconView2.setPreserveRatio(true);
        iconView2.setFitWidth(100);
        iconView2.setFitHeight(100);
        icon2.setGraphic(iconView2);

        Label icon3 = new Label();
        ImageView iconView3 = new ImageView(new Image(new FileInputStream("C:\\Users\\Emanuel Gadalean\\Links\\download.jpg")));
        iconView3.setPreserveRatio(true);
        iconView3.setFitWidth(100);
        iconView3.setFitHeight(100);
        icon3.setGraphic(iconView3);

        Label icon4 = new Label();
        ImageView iconView4 = new ImageView(new Image(new FileInputStream("C:\\Users\\Emanuel Gadalean\\Links\\poza3.jpg")));
        iconView4.setPreserveRatio(true);
        iconView4.setFitWidth(100);
        iconView4.setFitHeight(100);
        icon4.setGraphic(iconView4);

        Label icon5 = new Label();
        ImageView iconView5 = new ImageView(new Image(new FileInputStream("C:\\Users\\Emanuel Gadalean\\Links\\poza 4.jpg")));
        iconView5.setPreserveRatio(true);
        iconView5.setFitWidth(100);
        iconView5.setFitHeight(100);
        icon5.setGraphic(iconView5);


        VBox vbox = new VBox(icon1, icon2, icon3, icon4, icon5);
        vbox.setSpacing(10);

        return vbox;
    }
}
