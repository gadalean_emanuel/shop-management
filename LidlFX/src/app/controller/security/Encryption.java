package app.controller.security;

public class Encryption {

    public static String encryption(String password) {
        String encryptedPassword = "";
        for (int i = 0; i < password.length(); i++) {
            int asciiCode = password.charAt(i);
            encryptedPassword += Character.toString(asciiCode + 3);
        }
        return encryptedPassword;
    }

    public static String decryption(String password) {
        String encryptedPassword = "";
        for (int i = 0; i < password.length(); i++) {
            int asciiCode = password.charAt(i);
            encryptedPassword += Character.toString(asciiCode - 3);
        }
        return encryptedPassword;
    }
}
