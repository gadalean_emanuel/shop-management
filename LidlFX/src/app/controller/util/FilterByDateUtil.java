package app.controller.util;

import app.model.Expense;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class FilterByDateUtil<E extends HasDate> {

    public List<E> filterObjectsByDateInterval(LocalDate startDate, LocalDate endDate, List<E> objects) {
        if (startDate == null && endDate == null) return objects;

        List<E> filteredList = new ArrayList<>();
        for (E object : objects) {
            if (startDate == null && object.getDate().compareTo(endDate) <= 0) {
                filteredList.add(object);
            } else if (endDate == null && object.getDate().compareTo(startDate) >= 0) {
                filteredList.add(object);
            } else if (object.getDate().compareTo(startDate) >= 0 && object.getDate().compareTo(endDate) <= 0) {
                filteredList.add(object);
            }
        }
        return filteredList;
    }
}
