package app.controller.util;

import java.time.LocalDate;

public interface HasDate {

    LocalDate getDate();
}
