package app.controller;

import app.controller.util.FilterByDateUtil;
import app.repository.constants.DateFormat;
import app.model.*;
import app.repository.*;
import app.repository.constants.UserRoles;
import app.controller.security.Encryption;

import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

public class Controller {

    private final UsersRepository usersRepository;

    private final ShopRepository shopRepository;

    private final EncashmentRepository encashmentRepository;

    private final ExpenseRepository expenseRepository;

    private final ProductRepository productRepository;

    public Controller(UsersRepository usersRepository, ShopRepository shopRepository, EncashmentRepository encashmentRepository, ExpenseRepository expenseRepository, ProductRepository productRepository) {
        this.usersRepository = usersRepository;
        this.shopRepository = shopRepository;
        this.encashmentRepository = encashmentRepository;
        this.expenseRepository = expenseRepository;
        this.productRepository = productRepository;
    }

    public User getUserByUsernameAndPassword(String username, String password) throws IllegalArgumentException {
        Optional<User> searchedUser = usersRepository.findAll().stream()
                .filter(user -> user.getUsername().equals(username) && user.getPassword().equals(Encryption.encryption(password)))
                .findFirst();
        if (searchedUser.isEmpty()) throw new IllegalArgumentException();
        else return searchedUser.get();
    }

    /**
     * @param user - logged user which can work/manage one or multiple shops
     * @return - shops that the user manages/ works for
     */
    public List<Shop> findShopsAvailableForUser(User user) {
        List<Shop> shopsAvailableForEmployee = shopRepository.findAll().stream()
                .filter(shop -> shop.getEmployees().contains(user))
                .collect(Collectors.toList());
        List<Shop> shopsAvailableForManager = shopRepository.findAll().stream()
                .filter(shop -> shop.getManagers().contains(user))
                .collect(Collectors.toList());
        if (shopsAvailableForEmployee.isEmpty()) return shopsAvailableForManager;
        else return shopsAvailableForEmployee;
    }

    public List<Shop> findAllShops() {
        return shopRepository.findAll();
    }

    public void insertShop(String name, String address) {
        Shop newShop = new Shop();
        newShop.setName(name);
        newShop.setAddress(address);
        shopRepository.insert(newShop);
    }

    public void updateShop(Shop shop, String name, String address) {
        shop.setName(name);
        shop.setAddress(address);
        shopRepository.update(shop);
    }

    public void insertManager(String name, int age, long CNP, String gender, String address, String username, String password, Shop shop) {
        User newManager = new User(usersRepository.findAll().size() + 1, name, age, CNP, gender, address, username, Encryption.encryption(password), UserRoles.Manager.name(), "Activ");
        usersRepository.insert(newManager);

        shop.getManagers().add(newManager);
        shopRepository.updateShopUsersRelation(shop);
    }

    public List<User> findAllUsers() {
        return usersRepository.findAll();
    }

    public void updateManager(User manager, String name, int age, long CNP, String gender, String address, String username, String password, Shop shop) {
        manager.setName(name);
        manager.setAge(age);
        manager.setCNP(CNP);
        manager.setGender(gender);
        manager.setAddress(address);
        manager.setUsername(username);
        manager.setPassword(Encryption.encryption(password));

        usersRepository.update(manager);

        shopRepository.updateManagerFromShop(shop, manager);
    }

    public List<Expense> findAllExpenses() {
        return expenseRepository.findAll();
    }

    public List<User> findEmployeesByShopId(int id) {
        return shopRepository.findAllEmployeesByShopId(id);
    }

    public void insertEmployee(String name, int age, long CNP, String gender, String address, String username, String password, Shop shop) {
        User newEmployee = new User(usersRepository.findAll().size() + 1, name, age, CNP, gender, address, username, Encryption.encryption(password), UserRoles.Employee.name(), "Activ");
        usersRepository.insert(newEmployee);

        shop.getEmployees().add(newEmployee);
        shopRepository.updateShopUsersRelation(shop);
    }

    public void updateEmployee(User employee, String name, int age, long CNP, String gender, String address, String username, String password, Shop shop) {
        employee.setName(name);
        employee.setAge(age);
        employee.setCNP(CNP);
        employee.setGender(gender);
        employee.setAddress(address);
        employee.setUsername(username);
        employee.setPassword(Encryption.encryption(password));

        usersRepository.update(employee);

        shopRepository.updateEmployeeFromShop(shop, employee);
    }

    public void removeEmployee(User employee, Shop shop) {
        shop.getEmployees().remove(employee);
        shopRepository.updateShopUsersRelation(shop);

        usersRepository.delete(employee);
    }

    public void removeManager(User manager, Shop shop) {
        shop.getManagers().remove(manager);
        shopRepository.updateShopUsersRelation(shop);

        usersRepository.delete(manager);
    }

    public List<Product> findAllProducts() {
        return productRepository.findAll();
    }

    public List<Product> findProductsByShopId(int shopId) {
        return productRepository.findAllByShopId(shopId);
    }

    public void insertProduct(String name, float cost, String unit, int quantity, int shopId) {
        Product newProduct = new Product();
        newProduct.setName(name);
        newProduct.setCost(cost);
        newProduct.setUnit(unit);
        newProduct.setQuantityInContainers(quantity);
        newProduct.setShopId(shopId);

        productRepository.insert(newProduct);
    }

    public void removeProduct(Product product) {
        productRepository.delete(product);
    }

    public void updateProduct(Product product, String name, float cost, String unit, int quantity, int shop_id) {
        product.setName(name);
        product.setCost(cost);
        product.setUnit(unit);
        product.setQuantityInContainers(quantity);
        product.setShopId(shop_id);

        productRepository.update(product);
    }

    public void removeExpense(Expense expense) {
        expenseRepository.deleteExpense(expense);
    }

    public void insertExpense(String name, float cost, LocalDate date, int shopId) {
        Expense expense = new Expense(name, cost, date, shopId);
        expenseRepository.insertExpense(expense);
    }

    public void updateExpense(Expense expense, String name, float cost, LocalDate date) {
        expense.setName(name);
        expense.setCost(cost);
        expense.setDate(date);

        expenseRepository.updateExpense(expense);
    }

    public void removeEncashment(Encashment encashment) {
        encashmentRepository.deleteEncashment(encashment);
    }

    public void updateEncashment(Encashment encashment, String numOfProd, String value) {
        encashment.setNumOfProducts(Integer.parseInt(numOfProd));
        encashment.setValue(Float.parseFloat(value));
        encashmentRepository.updateEncashment(encashment);
    }

    public Product findProductByShopId(int shopId, String productName) {
        return productRepository.findProductByShopIdAndByProductName(shopId, productName);
    }

    public List<Encashment> findAllEncashmentsByShopId(int shopId) {
        return encashmentRepository.findAllByShopId(shopId);
    }

    public void insertEncashmentToAShop(String prodName, String cost, String numOfProd, String date, int shopId) {
        Encashment encashment = new Encashment(Float.parseFloat(cost), LocalDate.parse(date, DateFormat.DATE_FORMATTER), prodName, Integer.parseInt(numOfProd), shopId);

        encashmentRepository.insertEncashment(encashment);
    }

    /**
     * update quantity of a product in a shop
     *
     * @param productName -
     * @param quantity    - quantity of the product
     * @param shopId      -
     */
    public void updateDeposit(String productName, int quantity, int shopId) {
        for (Product product : findProductsByShopId(shopId)) {
            if (product.getName().equals(productName)) {
                product.setQuantityInContainers(product.getQuantityInContainers() + quantity);
                productRepository.update(product);
            }
        }
    }

    public List<Expense> findAllExpensesByShopId(int id) {
        return expenseRepository.findAllByShopId(id);
    }

    public List<User> findAllManagersByShopId(int id) {
        return shopRepository.findAllManagersByShopId(id);
    }

    public List<Expense> filterShopExpensesByDateInterval(LocalDate startDate, LocalDate endDate, int shopId) {
        return new FilterByDateUtil<Expense>().filterObjectsByDateInterval(startDate, endDate, findAllExpensesByShopId(shopId));
    }

    public List<Encashment> filterEncashmentById(LocalDate startDate, LocalDate endDate, int shopId) {
        return new FilterByDateUtil<Encashment>().filterObjectsByDateInterval(startDate, endDate, findAllEncashmentsByShopId(shopId));
    }
}
