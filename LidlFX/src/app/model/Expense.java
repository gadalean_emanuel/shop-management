package app.model;

import app.controller.util.HasDate;

import java.time.LocalDate;

public class Expense extends ExpenseInformation implements HasDate {

    private int id;

    private LocalDate date;

    public Expense(String name, float cost, LocalDate date, int shopId) {
        setName(name);
        setCost(cost);
        this.date = date;
        setShopId(shopId);
    }

    public Expense(int id, String name, float cost, LocalDate date, int shopId) {
        setName(name);
        setCost(cost);
        this.date = date;
        this.id = id;
        setShopId(shopId);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) return false;
        if (this == obj) return true;
        if (obj.getClass() != getClass()) return false;

        Expense expense = (Expense) obj;
        return expense.getId() == getId();
    }
}
