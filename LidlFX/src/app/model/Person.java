package app.model;

public abstract class Person {

    private String name;

    private int age;

    private long CNP;

    private String gender;

    private String address;

    public Person(String name, int age, long CNP, String gender, String address) {
        this.name = name;
        this.age = age;
        this.CNP = CNP;
        this.gender = gender;
        this.address = address;
    }

    public Person() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public long getCNP() {
        return CNP;
    }

    public void setCNP(long CNP) {
        this.CNP = CNP;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public String toString() {
        return name + ',' + age + "," + CNP +
                "," + gender +
                "," + address;
    }
}
