package app.model;

public class User extends Person {

    private int id;

    private String username;

    private String password;

    private String userRole;

    private String userStatus;

    public User(int id, String name, int age, long CNP, String gender, String address, String username, String password, String userRole, String userStatus) {
        super(name, age, CNP, gender, address);
        this.username = username;
        this.password = password;
        this.userRole = userRole;
        this.userStatus = userStatus;
        this.id = id;
    }

    public User() {
    }

    public User(int id, String name, int age, long CNP, String gender, String address) {
        super(name, age, CNP, gender, address);
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUserRole() {
        return userRole;
    }

    public void setUserRole(String userRole) {
        this.userRole = userRole;
    }

    public String getUserStatus() {
        return userStatus;
    }

    public void setUserStatus(String userStatus) {
        this.userStatus = userStatus;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj == null) return false;
        if(this == obj) return true;
        if(obj.getClass() != getClass()) return false;

        User user = (User) obj;
        return user.getId() == getId();
    }

}
