package app.model;

public class Product extends ExpenseInformation {

    private int id;

    private String unit;

    private int quantityInContainers;

    public Product(int id, String name, float cost, String unit, int quantityInContainers) {
        this.id = id;
        super.setName(name);
        super.setCost(cost);
        this.unit = unit;
        this.quantityInContainers = quantityInContainers;
    }

    public Product() {

    }

    public Product(int id, String name, float cost, String unit, int quantity_in_containers, int shopIdField) {
        this(id, name, cost, unit, quantity_in_containers);
        setShopId(shopIdField);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getQuantityInContainers() {
        return quantityInContainers;
    }

    public void setQuantityInContainers(int quantityInContainers) {
        this.quantityInContainers = quantityInContainers;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) return false;
        if (this == obj) return true;
        if (obj.getClass() != getClass()) return false;

        Product product = (Product) obj;
        return product.getId() == getId();
    }
}
