package app.model;

import app.controller.util.HasDate;

import java.time.LocalDate;

public class Encashment implements HasDate {

    private int id;

    private float value;

    private String productName;

    private int numOfProducts;

    private LocalDate date;

    private int shopId;

    public Encashment(int id, float value, String productName, int numOfProducts, LocalDate date, int shopId) {
        this.value = value;
        this.date = date;
        this.productName = productName;
        this.numOfProducts = numOfProducts;
        this.id = id;
        this.shopId = shopId;
    }

    public Encashment(float value, LocalDate date, String productName, int numOfProducts, int shopId) {
        this.value = value;
        this.date = date;
        this.productName = productName;
        this.numOfProducts = numOfProducts;
        this.shopId = shopId;
    }

    public float getValue() {
        return value;
    }

    public void setValue(float value) {
        this.value = value;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public int getNumOfProducts() {
        return numOfProducts;
    }

    public void setNumOfProducts(int numOfProducts) {
        this.numOfProducts = numOfProducts;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getShopId() {
        return shopId;
    }

    public void setShopId(int shopId) {
        this.shopId = shopId;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) return false;
        if (this == obj) return true;
        if (obj.getClass() != getClass()) return false;

        Encashment encashment = (Encashment) obj;
        return encashment.getId() == getId();
    }
}
