module LidlFX {

    requires javafx.fxml;
    requires  javafx.controls;
    requires  javafx.graphics;
    requires java.sql;

    opens app;
    opens app.model to javafx.base;
}